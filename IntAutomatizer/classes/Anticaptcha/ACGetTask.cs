﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntAutomatizer.classes.Anticaptcha
{
    class ACGetTask
    {
        public ACGetTask(string clientKey,int taskId)
        {
            this.clientKey = clientKey;
            this.taskId = taskId;
        }
        public string clientKey;
        public int taskId;
    }
}
