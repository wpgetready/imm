﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntAutomatizer.classes.Anticaptcha
{
   

    public class ACGetTaskResult
    {
        public int errorId;
        public string errorCode;
        public string errorDescription;
        public string status;
        public Solution solution;
        public Double cost;
        public string ip;
        public int createTime;
        public int endTime;
        public int solveCount;
    }

    /// <summary>
    /// WARNING: pay attenion this solution object is ONY for Recaptcha cases. For other solutions
    /// the object differs..
    /// </summary>
    public class Solution
    {
        public string gRecaptchaResponse;
        public string gRecaptchaResponseMD5;
    }
}
