﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntAutomatizer.classes.Anticaptcha
{
    public class ACRequestTask
    {
        public string clientKey { get; set; }
        public Task task { get; set; }
        public int softId { get; set; }
        public string languagePool { get; set; }
    }

    public class Task
    {
        public string type { get; set; }
        public string websiteURL { get; set; }
        public string websiteKey { get; set; }
    }

}

/*EXAMPLE
 {
"clientKey":"dce6bcbb1a728ea8d871de6d169a2057",
"task":
{
    "type":"NoCaptchaTaskProxyless",
    "websiteURL":"http:\/\/mywebsite.com\/recaptcha\/test.php",
    "websiteKey":"6Lc_aCMTAAAAABx7u2N0D1XnVbI_v6ZdbM6rYf16"
},
"softId":0,
"languagePool":"en"
}
*/

