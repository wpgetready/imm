﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntAutomatizer.classes.Anticaptcha
{
    //https://anticaptcha.atlassian.net/wiki/display/API/createTask+%3A+captcha+task+creating
    public class ACRequestTaskResult
    {
        public int errorId;
        public string errorCode;
        public string errorDescription;
        public int taskId;
    }
}
