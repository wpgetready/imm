﻿using System;
using System.Collections.Generic;
using System.Drawing.Design;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace IntAutomatizer.classes.Anticaptcha
{

    /// <summary>
    /// The 'new' (I suppose in the future won't be new anyway) AntiCaptcha, deals with the fact of calling
    /// an external API and getting results to be used in the main program.
    /// 
    /// We need 
    /// 1) Call the service, wait for an answer
    /// This is explained in 
    /// https://anticaptcha.atlassian.net/wiki/display/API/createTask+%3A+captcha+task+creating
    /// 
    /// 2) call AGAIN to retrieve the answer
    /// Using the answer provided, we call again to get the result.
    /// So we know the IMPORTANCE OF NOT LOSING THE ANSWER!!!!
    /// For what I see , you can't retrieve the data after you lost.
    /// 
    /// </summary>
    public class AntiCaptcha
    {
        //20170730: Nueva API a invocar para resolver ReCaptcha,me la dan cuando me registro
        private string antiCaptchaKey = "2742b197d81ca73dc156749414267b23";

        private string url_AC_createTask = "https://api.anti-captcha.com/createTask";
        private string url_AC_getTaskResult = "https://api.anti-captcha.com/getTaskResult";

        private string url_infracciones = "http://www.montevideo.gub.uy/consultainfracciones";
        //La siguiente es la clave que tiene el sitio de la Intendencia HARDCODED
        //En realidad NO DEBERIA ponerse aquí sino extraerse de la página, pero a los efectos de los primeros test es aceptable.
        private string imm_web_key = "6LdpfSMUAAAAADeYPGmMZagjN2pYjX1MH5Hi9pDC";

        public int secondsElapsed { get; set; }


        /// <summary>
        /// Invokes Anti-Captcha API and handles all the issues (if possible)
        /// V2 if the status is processing we need to wait until status is ready.
        /// V2.01: error control. If createTask gives an error, returns null.
        /// </summary>
        public ACGetTaskResult invokeAntiCaptcha()
        {
            int secondsCounter = 0;
            ACGetTaskResult result = null;
            bool completed = false;
            Utilities.Log(("start invokeAntiCaptcha please wait..."));
            ACRequestTaskResult taskResult = createTask();
            if (taskResult == null) return null;
            switch (taskResult.errorId)
            {
                case 0: //no error
                    //Task completed successfully,get task details
                    completed = false;
                    while (!completed)
                    {
                        result = getTaskDetails(taskResult.taskId);
                        //in case of error, return null as a message
                        if (result == null) return null;
                        
                        if (result.status != "ready")
                        {
                            secondsCounter += 12; //seconds counter for final result
                            System.Threading.Thread.Sleep(12000); //wait twelve seconds
                            continue;
                        }
                        completed = true;
                    }
                    break;

                default: //some kind of error when retrieveng task
                    Utilities.Log(string.Format("Error when completing task: {0} - {1} - {2}", 
                        taskResult.errorId,taskResult.errorCode,taskResult.errorDescription));
                    break;
            }
            Utilities.Log(string.Format("Completed in {0} seconds",secondsCounter));
            secondsElapsed = secondsCounter;
            return result;
        }


        //For JSON use:
        // http://www.newtonsoft.com/json/help/html/DeserializeObject.htm
        /// <summary>
        /// TODO: handle errors
        /// V2: added error handling (because an error happened...)
        /// In case of error it returns null
        /// </summary>
        /// <returns></returns>
        public ACRequestTaskResult createTask()
        {
            string url = url_infracciones;
            ACRequestTask req = createAcRequest(antiCaptchaKey, imm_web_key, url);
            string output = "";
            string json = JsonConvert.SerializeObject(req, Formatting.Indented);
            try
            {
                output = invokeService(json, url_AC_createTask);
            }
            catch (WebException we)
            {
                Utilities.Log(string.Format("invokeService error en createTask: {0}", we.Message));
                return null;
            }
            
            ACRequestTaskResult result = JsonConvert.DeserializeObject<ACRequestTaskResult>(output);
            Utilities.Log("Creating task...");
            Utilities.Log(output);
            return result;
        }

        /// <summary>
        /// Giving the Task id result from the create task, get the operation result.
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public ACGetTaskResult getTaskDetails(int taskId)
        {
            ACGetTask task = new ACGetTask(antiCaptchaKey, taskId);
            string json = JsonConvert.SerializeObject(task, Formatting.Indented);
            string output = "";

            try
            {
                output = invokeService(json, url_AC_getTaskResult);
            }
            catch (WebException we)
            {
                Utilities.Log(string.Format("getTaskDetails error en createTask: {0}", we.Message));
                return null;
            }
            
            ACGetTaskResult result = JsonConvert.DeserializeObject<ACGetTaskResult>(output);
          //  Utilities.Log("Requesting taskId " + taskId); commented since we are using a loop to avoid unnecesary log data
          //  Utilities.Log(output);
            return result;
        }

        /// <summary>
        /// giving the json data and the url, invoke the service and return json response.
        /// </summary>
        /// <param name="json"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        private string invokeService(string json, string url)
        {
            var httpWebRequest = (HttpWebRequest) WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse) httpWebRequest.GetResponse();

            string output = "";
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                output = streamReader.ReadToEnd();
            }
            return output;
        }

        /// <summary>
        /// Create request to send AntiCaptcha
        /// Notice request seems to be INDEPENDENT of what recaptcha is doing in the page.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        private ACRequestTask createAcRequest(string key, string sitekey,string url)
        {
            ACRequestTask req = new ACRequestTask();
            req.clientKey = key;
            Task t= new Task();
            t.type = "NoCaptchaTaskProxyless";
            t.websiteURL = url;
            t.websiteKey = sitekey;
            req.task = t;
            req.languagePool = "en";
            req.softId = 0;
            return req;
        }
    }
}

/*EXAMPLE
 {
"clientKey":"dce6bcbb1a728ea8d871de6d169a2057",
"task":
{
    "type":"NoCaptchaTaskProxyless",
    "websiteURL":"http:\/\/mywebsite.com\/recaptcha\/test.php",
    "websiteKey":"6Lc_aCMTAAAAABx7u2N0D1XnVbI_v6ZdbM6rYf16"
},
"softId":0,
"languagePool":"en"
}
*/
