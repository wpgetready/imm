﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntAutomatizer.classes.Anticaptcha
{
    public class jsonResponse
    {
        public string command { get; set; }
        public Settings settings { get; set; }
        public bool merge { get; set; }
        public string method { get; set; }
        public object selector { get; set; }
        public string data { get; set; }
    }

    public class AjaxPageState
    {
        public string theme { get; set; }
        public string theme_token { get; set; }
    }

    public class ApachesolrAutocomplete
    {
        public string path { get; set; }
    }

    public class Paths
    {
        public string admin { get; set; }
        public string non_admin { get; set; }
    }

    public class Overlay
    {
        public Paths paths { get; set; }
        public List<string> pathPrefixes { get; set; }
        public string ajaxCallback { get; set; }
    }

    public class Settings
    {
        public string basePath { get; set; }
        public string pathPrefix { get; set; }
        public AjaxPageState ajaxPageState { get; set; }
        public ApachesolrAutocomplete apachesolr_autocomplete { get; set; }
        public Overlay overlay { get; set; }
    }
    }
