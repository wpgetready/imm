﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

    class CSResult
    {
        private string _ResultCode, _MajorID, _MinorID, _Type, _Timeout, _Text;

        public CSResult(string resultCode, string majorID, string minorID, string type, string timeout, string text)
        {
            this._ResultCode = resultCode;
            this._MajorID = majorID;
            this._MinorID = minorID;
            this._Type = type;
            this._Timeout = timeout;
            this._Text = text;
        }


        public string ResultCode
        {
            get
            {
                return _ResultCode;
            }
            set
            {
                _ResultCode = value;
            }
        }


        public string MajorID
        {
            get
            {
                return _MajorID;
            }
            set
            {
                _MajorID = value;
            }
        }


        public string MinorID
        {
            get
            {
                return _MinorID;
            }
            set
            {
                _MinorID = value;
            }
        }


        public string Type
        {
            get
            {
                return _Type;
            }
            set
            {
                _Type = value;
            }
        }


        public string Timeout
        {
            get
            {
                return _Timeout;
            }
            set
            {
                _Timeout = value;
            }
        }


        public string Text
        {
            get
            {
                return _Text;
            }
            set
            {
                _Text = value;
            }
        }

    }
