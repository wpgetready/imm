﻿using System;
using System.Text;
using System.Drawing;
using System.Net;
using System.IO;
using System.Drawing.Imaging;
using System.Threading;
using System.Windows.Forms;


    class CaptchaSniper
    {

        private string Uri = "http://127.0.0.1:80/";

        private string username = "";
        private string password = "";
        private string referer = "";
        private CSResult lastResult;
        private byte[] lastBmp;
        private bool flag;

    /// <summary>
    /// In this current version, the class mimics Decaptcher, but I never used it so usr & pwd can be ignored.
    /// </summary>
        public CaptchaSniper()
    {
        username = "none";
        password = "none";
    }

        /// <summary>
        /// Creates new instance of Decaptcher class
        /// </summary>
        /// <param name="_username">username for users decaptcher account</param>
        /// <param name="_password">password for users decaptcher account</param>
        public CaptchaSniper(string _username, string _password)
        {
            username = _username;
            password = _password;
        }

        /// <summary>
        /// Creates new instance of Decaptcher class
        /// </summary>
        /// <param name="_username">username for users decaptcher account</param>
        /// <param name="_password">password for users decaptcher account</param>
        /// <param name="_referer">referer ID, if you have setup a affilate account.</param>
        public CaptchaSniper(string _username, string _password, string _referer)
        {
            referer = _referer;
            username = _username;
            password = _password;
        }

        /* this solution works, however, I need FIRST to get the image and then process it.
         * The reason is simple: while more captcha are requested, more will be solved.
         * Unsolved one will going to a pool, to be manually solved if I wanted.
         * future versions, will check first solved solutions , and if there is already solved, provide the answer.
         * Yeah it can be gigantic but ratio will be greater along the time.
         */
        public CSResult GetCaptchaSolved(string FileUrl)
        {
            HttpWebRequest request = ((HttpWebRequest)(WebRequest.Create(FileUrl)));
            request.Method = "GET";
            HttpWebResponse response = ((HttpWebResponse)(request.GetResponse()));
            Stream requestStream = response.GetResponseStream();
            int streamLength = Convert.ToInt32(response.ContentLength);
            byte[] fileData = new byte[] {
                Convert.ToByte(streamLength)};
            requestStream.Read(fileData, 0, streamLength);
            requestStream.Close();
            return GetCaptchaSolved(fileData);
        }

        /// <summary>
        /// Custom-made solution to solve local files.
        /// Notice that localFile is never used, is to take advantage of overloading since there is another method with same parameter if I use string
        /// </summary>
        /// <param name="imagePath"></param>
        /// <param name="localFile"></param>
        /// <returns></returns>
        public CSResult GetCaptchaSolved(string imagePath, bool localFile)
        {
            Bitmap img = (Bitmap)Image.FromFile(imagePath);
            return GetCaptchaSolved(img);
        }

        public CSResult GetCaptchaSolved(byte[] File)
        {
            try
            {
                HttpWebRequest request = ((HttpWebRequest)(WebRequest.Create(Uri)));
                request.Method = "POST";
                request.KeepAlive = true;

                string boundary = ("-------------------------" + DateTime.Now.Ticks.ToString("x"));
                string header = ("\r\n" + ("--"
                            + (boundary + "\r\n")));
                string footer = ("\r\n" + ("--"
                            + (boundary + "\r\n")));
                request.ContentType = string.Format("multipart/form-data; boundary={0}", boundary);
                StringBuilder contents = new StringBuilder();
                contents.Append("\r\n");
                contents.Append(header);
                contents.Append(("Content-Disposition: form-data; name=\"function\"" + "\r\n"));
                contents.Append("\r\n");
                contents.Append("picture2");
                contents.Append(header);
                contents.Append(("Content-Disposition: form-data; name=\"username\"" + "\r\n"));
                contents.Append("\r\n");
                contents.Append(username);
                contents.Append(header);
                contents.Append(("Content-Disposition: form-data; name=\"password\"" + "\r\n"));
                contents.Append("\r\n");
                contents.Append(password);
                contents.Append(header);
                contents.Append(("Content-Disposition: form-data; name=\"pict_to\"" + "\r\n"));
                contents.Append("\r\n");
                contents.Append("0");
                contents.Append(header);
                contents.Append(("Content-Disposition: form-data; name=\"pict_type\"" + "\r\n"));
                contents.Append("\r\n");
                contents.Append(referer);
                contents.Append(header);
                contents.Append(("Content-Disposition: form-data; name=\"pict\"; filename=\"Untitled.jpg\"" + "\r\n"));
                contents.Append(("Content-Type: image/jpeg" + "\r\n"));
                contents.Append("\r\n");
                byte[] BodyBytes = Encoding.UTF8.GetBytes(contents.ToString());
                byte[] footerBytes = Encoding.UTF8.GetBytes(footer);
                request.ContentLength = (BodyBytes.Length
                            + (File.Length + footerBytes.Length));
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(BodyBytes, 0, BodyBytes.Length);
                requestStream.Write(File, 0, File.Length);
                requestStream.Write(footerBytes, 0, footerBytes.Length);
                requestStream.Flush();
                requestStream.Close();
                request.Timeout = 300000;
                string ret = (new StreamReader(request.GetResponse().GetResponseStream()).ReadToEnd());
                return new CSResult(ret.Split('|')[0], ret.Split('|')[1], ret.Split('|')[2], ret.Split('|')[3], ret.Split('|')[4], ret.Split('|')[5]);
            }
            catch
            {
                return null;
            }
        }

      

        private byte[] BmpToBytes(Bitmap bmp)
        {
            MemoryStream m = new MemoryStream();
            bmp.Save(m, ImageFormat.Jpeg);

            byte[] output = m.GetBuffer();
            bmp.Dispose();
            m.Close();

            return output;
        }


        public CSResult GetCaptchaSolved(Bitmap captchaBmp)
        {
            lastBmp = BmpToBytes(captchaBmp);

            flag = false;
            Thread n = new Thread(new ThreadStart(GetCaptchaSolvedThread));
            n.Start();

            while (!flag)
            {
                Application.DoEvents();
            }

            return lastResult;
        }


        private void GetCaptchaSolvedThread()
        {
            lastResult = GetCaptchaSolved(lastBmp);
            flag = true;
        }

    /// <summary>
    /// Se me escapa porque el metodo TIENE que estar aqui , no tiene sentido ni necesidad, sin embargo rehusa a compilarse si es heredado(?!?!?!)
    /// </summary>
    /// <param name="captchaAnswer"></param>
    /// <returns></returns>
    public bool isValidAnswer(string captchaAnswer)
    {
        return (captchaAnswer.Length == 5);
    }


}

/// <summary>
/// This class was built to separate CS's functionality from my own code
/// </summary>
class infraccionesCaptchaSniper: CaptchaSniper
{
    /// <summary>
    /// Valid answer are those which answer has exactly 5 characters.
    /// Se me escapa como es posible que este metodo no sea visto.
    /// </summary>
    /// <param name="captchaAnswer"></param>
    /// <returns></returns>
    public bool isValidAnswer(string captchaAnswer)
    {
        return (captchaAnswer.Length == 5);
     }
}


