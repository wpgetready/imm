﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

public  class Utilities
    {
    //This is where we would store all the work
    //There are the following directories:
    //data- sqlite storag
    //OBSOLETE: captcha - captcha storage
    //pictures - image fine storage
    //responses- responses in txt , for further analysis

    //removido de Dropbox
    //public static string BASE_DIR = @"D:\RenoVektor\Dropbox\WORK\20170105-scrapImpo\scrapImpo\scrapImpo\IntAutomatizer\DATA";
    //20170213: Eliminadas todas las referencias disperas de BASE_DIR y concentradas aquí. Ahora es posible si quiero
    //cambiar los caminos de cualquier carpeta , por ejemplo si deseo que estén fuera de Dropbox.

    public static string BASE_DIR = @"D:\ESPACIO-TRABAJO\20170110-multas-intendencia\data";
    public static string BASE_DIR_CAPTCHA = Path.Combine(BASE_DIR, "captcha");  //OBSOLETE
    public static string BASE_DIR_DATABASE = Path.Combine(BASE_DIR, "database");
    public static string BASE_DIR_RESPONSES = Path.Combine(BASE_DIR, "responses");
    public static string BASE_DIR_IMAGES = Path.Combine(BASE_DIR, "images");

    public static string PROXY = "http://proxysis:8080";
    public static Regex MyRegex = new Regex( "https://www.impo.com.uy/bases/notificaciones-cgm/(.*?)-2017/1",
    RegexOptions.IgnoreCase | RegexOptions.CultureInvariant| RegexOptions.IgnorePatternWhitespace
    | RegexOptions.Compiled     );
    public static string urlImpo = "https://www.impo.com.uy/bases/notificaciones-cgm/{0}-2017/1";
    public static string logFile = "log.txt";

        /// <summary>
        /// processResult describes how the things were after trying getting images from server
        /// NoProcessed is the default, Success if we retrieved at least one images,
        /// SuccessNoImages is an special case of success
        /// Latsly, error means there was a problem retrieving images, so no images.
        /// </summary>
        public enum processResult
        {
            NoProcessed,
            Success,
            SuccessNoImages,
            Error
        };


    //Notar que el año VARIA, asi que hay que mejorar esta rutina para que contemple el año, cosa que aun no hace.
    //Notar ademas que mientras es asi, el reset solo explorar el año 2017

    /// <summary>
    /// Get a page from Internet,return string with content.
    /// </summary>
    /// <param name="Url"></param>
    /// <returns></returns>
    public static String getPage(string Url)
        {
            WebClient myWebClient = new WebClient();

            if (isThereAPRoxy())
            {
                myWebClient.Proxy = getProxy();
            }
            myWebClient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)"); // If you need to simulate a specific browser
            myWebClient.Headers.Add("accept-charset", "windows-1252");

            byte[] myDataBuffer = myWebClient.DownloadData(Url);
            string download = Encoding.Default.GetString(myDataBuffer);
            return download;
        }

    /// <summary>
    /// Used on environments with proxy
    /// </summary>
    /// <returns></returns>
    public static IWebProxy getProxy()
    {
        WebProxy wp = new WebProxy(PROXY);
        wp.UseDefaultCredentials = true;
        return wp;
    }

    /// <summary>
    /// Checks if there is an active proxy.
    /// </summary>
    /// <returns></returns>
    public static bool isThereAPRoxy()
    {
        bool useProxy = !string.Equals(System.Net.WebRequest.DefaultWebProxy.GetProxy(new Uri(PROXY)), PROXY);
        return useProxy;
    }

    /// <summary>
    ///Make a Post to the expected site, return page after invoke. 
    /// </summary>
    /// <param name="msg"></param>
    /// <param name="url_infracciones"></param>
    /// <returns></returns>
    public static string postCall(string msg, string url_infracciones)
    {
        var postData = Encoding.ASCII.GetBytes(msg);
        var request = (HttpWebRequest)WebRequest.Create(url_infracciones);
        if (Utilities.isThereAPRoxy())
        {
           request.Proxy = getProxy();
        }
        request.Method = "POST";
        request.ContentType = "application/x-www-form-urlencoded";
        request.ContentLength = postData.Length;
        using (var stream = request.GetRequestStream())
        {
            stream.Write(postData, 0, postData.Length);
        }
        var response = (HttpWebResponse)request.GetResponse();
        return  new StreamReader(response.GetResponseStream()).ReadToEnd();
    }

    /*
        POST /system/ajax HTTP/1.1
        Host: www.montevideo.gub.uy
            User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0
        Accept: application/json, text/javascript, * / *; q=0.01
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Content-Type: application/x-www-form-urlencoded; charset=UTF-8
    X-Requested-With: XMLHttpRequest
    Referer: http://www.montevideo.gub.uy/consultainfracciones
    Content-Length: 9631
    Cookie: _ga=GA1.3.33027496.1484039461; dtPC=162546017_882h3; rxVisitor=1501321610943LA3EAH3BPEPJ9MD9T94J8TA5LHUCD2MR; has_js=1; dtLatC=3; _gid=GA1.3.341669414.1501321612; DRUPAL_UID=-1; _gat=1; dtCookie=84A99113839504F8169C0CD4A81D8164|X2RlZmF1bHR8MQ
    Connection: keep-alive
            */

        /// <summary>
        /// This first version tries to mimic invoke as similar as posible.
        /// However this can be an error, so be warned
        /// Let's see how it went
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="url_infracciones"></param>
        /// <returns></returns>
        public static string postCallAjax(string msg, string url_infracciones)
        {
            var postData = Encoding.ASCII.GetBytes(msg);
            var request = (HttpWebRequest)WebRequest.Create(url_infracciones);
            if (Utilities.isThereAPRoxy())
            {
                request.Proxy = getProxy();
            }
           //que tan parecido tiene que ser el header para que funcione?
            request.Method = "POST"; //No es posible poner "POST /ajax" porque el método NO acepta espacios.
            //request.Host = consultaInfraccionesV2.url_sitio_web; already set when invoked
            request.UserAgent = "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0";
            request.Accept = "Accept: application/json, text/javascript, */*; q=0.01";
            request.Headers.Add(HttpRequestHeader.AcceptLanguage, "es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3");
            request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
            request.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            request.Headers.Add("X-Requested-With", "XMLHttpRequest");
            request.Referer = url_infracciones;
            request.ContentLength = postData.Length;
           // request.Connection = "keep-alive"; Esto no puede setearse por aquí dará una excepcion
            using (var stream = request.GetRequestStream())
            {
                stream.Write(postData, 0, postData.Length);
            }
            var response = (HttpWebResponse)request.GetResponse();
            return new StreamReader(response.GetResponseStream()).ReadToEnd();
        }
    /// <summary>
    /// Download an image from Web
    /// Source: http://stackoverflow.com/questions/3615800/download-image-from-the-site-in-net-c
    /// </summary>
    /// <param name="uri"></param>
    /// <param name="fileName"></param>
    public static void DownloadRemoteImageFile(string uri, string fileName, string folder)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            // Check that the remote file was found. The ContentType
            // check is performed since a request for a non-existent
            // image file might be redirected to a 404-page, which would
            // yield the StatusCode "OK", even though the image was not
            // found.
            if ((response.StatusCode == HttpStatusCode.OK ||
                response.StatusCode == HttpStatusCode.Moved ||
                response.StatusCode == HttpStatusCode.Redirect) &&
                response.ContentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
            {

            // if the remote file was found, download it
            if (!Directory.Exists(folder)) {
                Directory.CreateDirectory(folder);
            }
             fileName = Path.Combine(folder, fileName);

                using (Stream inputStream = response.GetResponseStream())
                using (Stream outputStream = File.OpenWrite(fileName))
                {
                    byte[] buffer = new byte[4096];
                    int bytesRead;
                    do
                    {
                        bytesRead = inputStream.Read(buffer, 0, buffer.Length);
                        outputStream.Write(buffer, 0, bytesRead);
                    } while (bytesRead != 0);
                }
            }
        }


        public static void writeFile(string filename, List<plate> multas)
        {
            string line;
            foreach (plate  m in multas)
            {
                line = "";
                //Assemble data
                line += string.Format("{0},{1},{2},{3},{4},{5}",
                                     m.plate_str, m.date, m.place, m.intervention, m.article, m.ur);
                //save line
                writeLine(filename, line);
            }
        }

    public static string readFile(string filename)
    {
        return  File.ReadAllText(filename);
    }

    public static void writeAllFile(string filename, string datos)
    {
        File.WriteAllText(filename, datos);
    }

        /// <summary>
        /// Writes a line into a filename
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="linea"></param>
        public static void writeLine(string filename, string linea)
        {
            Console.WriteLine(linea);
            using (StreamWriter file = File.AppendText(filename))
            {
                file.WriteLine(linea);
            }
        }

        /// <summary>
        /// Returns data between start and end
        /// if start is not found returns empty string
        /// if end is not found returns empty string
        /// Otherwise:fragment=extracted string. the function returns from the latest match and ahead.
        /// First version, there are tricky cases that will make this method fail. I'm not contemplating everything, you know.
        /// Second version: method returns remaining fragment (to looped analysis)
        /// Third version: there are cases where end hast too many matches before start. So we force to find AFTER the first match.
        /// </summary>
        /// <param name="toParse"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static string between(string toParse, string start, string end, out string fragment)
        {
            fragment = toParse;
            int intStart = toParse.IndexOf(start);
            if (intStart == -1) return string.Empty;

            int intEnd = toParse.IndexOf(end,intStart);
            if (intEnd == -1) return string.Empty;
            if (intEnd < intStart) return string.Empty; //end should be greater than start....
            fragment = toParse.Substring(intEnd + end.Length);
            return toParse.Substring(intStart + start.Length, intEnd - (intStart + start.Length));
        }

    /// <summary>
    /// input date in the format dd/mm/yyyy hh:mm
    /// output yyyymmddhhmm
    /// Improvement: Now I have unit testing, I will take another different approach, more robust
    /// Requirement: separate using a space
    /// Minutes have to be EXACTLY in the format dd:mm, other will fail
    /// invalid values will fail
    /// The problem is that I can refine indefinitely, so better to have an end to this...
    /// </summary>
    /// <param name="fecha"></param>
    /// <returns></returns>
    public static string reversedDate(string fecha)
    {
        string[] parts = fecha.Trim().Split(' ');
        //First part date , second part hour
        if (parts.Length != 2)
        {
            //Error here
        }
        string f = parts[0];
        string h = parts[1];


        string hour_cut = h.Trim().Replace(":", "");
        if (hour_cut.Length!=4) 
        {
            throw new Exception("Fecha invalida: el formato de tiempo es hh:mm");
        }

        string[] date_part = f.Split('/');
        if (date_part.Length != 3)
        {
            //Error here
            throw new Exception("Fecha invalida: tiene que estar en el formato dd/mm/yyyy");
        }
        //IMPORTANT: string.format will IGNORE COMPLETELY FORMAT IF WE DEAL WITH STRINGS, so we need to convert into integers first
        int month;
        if (!int.TryParse(date_part[1], out month))
        {
            //Error here
            throw new Exception("Fecha invalida: mes invalido");
        }

        int day;
        if (!int.TryParse(date_part[0], out day))
        {
            //Error here
            throw new Exception("Fecha invalida: dia invalido");
        }


        string reversed = string.Format("{0:D4}{1:d2}{2:d2}{3}", date_part[2], month, day, hour_cut);
        return reversed;
    }

    /// <summary>
    /// Extract current page number from current url. Return -1 if there is no match, -2 if result is a match
    /// but not a number.
    /// </summary>
    /// <param name="url"></param>
    /// <returns></returns>
    public static int getCounter(string url)
    {
        Match m = MyRegex.Match(url);
        if (m.Groups.Count < 2) return -1;
        int result;
        if (int.TryParse(m.Groups[1].Value.ToString(),out result))
        {
            return result;
        }
        return -2;
    }

    /// <summary>
    /// Builds a url page from pattern and a number.
    /// </summary>
    /// <param name="counter"></param>
    /// <returns></returns>
    public static string setUrlImpo (int counter)
    {
        return string.Format(urlImpo, counter);
    }

    public static void Log(string linea)
    {
        writeFile(logFile, linea);
    }

    /// <summary>
    /// Append line to a file.
    /// </summary>
    /// <param name="filename"></param>
    /// <param name="linea"></param>
    public static void writeFile(string filename, string linea)
    {
        Console.WriteLine(linea);
        using (StreamWriter file = File.AppendText(filename))
        {
            file.WriteLine(string.Format ("{0} - {1}" , DateTime.Now, linea));
        }
    }

        public static string loadFile(string filename)
        {
            if (!File.Exists(filename)) return null;
            return File.ReadAllText((filename));
        }

}
