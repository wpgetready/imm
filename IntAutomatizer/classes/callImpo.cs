﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading;

public delegate void MessageEventHandler(object sender, string msg);
/// <summary>
    /// This class makes all process needed to check one page on Impo Site
    /// 20170109: Improved: data is stored en Sqlite (!)
    /// It is not very good yet but improved. To be honest, I HATE making rutinary things as tables and queries and so on.
    /// 20170207: Automatizing as much as possible.
    /// </summary>
    public class callImpo
    {
    public event MessageEventHandler Msg;

    protected virtual void OnMsg(string msg)
    {
        if (Msg != null) {
            Msg(this, msg);
        }
    }

    /// <summary>
    /// Newest version, without need of requiring any pages. It will be inferred from the existig ones.
    /// </summary>
    public callImpo()
    {
    }

    public void start() { 
        string page = "";
        //Init connection to database
        DbConnection cn = sqlConnection.createConnection();
        pageCtl pc = new pageCtl(cn);
        plateCtl plate_ctl = new plateCtl(cn);
        string url = pc.getLastPage();   //get url of last page scanned

        List<string> rawData = null;

        if (url=="")
        {
            url = Utilities.setUrlImpo(1);
            OnMsg(string.Format("No se encontró una página en la base de datos, empezando por la primera... {0}", url));
        } else
        {
            OnMsg(string.Format("Ultima página detectada: {0}", url));
        }
    
        //Extract a counter from the url given.If there is a serious bug, exit now.
        int counter = Utilities.getCounter(url);
        switch (counter)
        {
            case -1:
                OnMsg(string.Format("Error GRAVE: no pude extraer el contador de  la url {0}",url));
                return;
            case -2:
                OnMsg(string.Format("Error GRAVE: la url no coincide con el formato esperado {0}", url));
                return;
            default:
                break;
        }

        do
        {
            counter++;
            url = Utilities.setUrlImpo(counter); //build url to make scrapping
            OnMsg(string.Format("Procesando página {0} ...",counter));
            OnMsg("Wait....");
            try
            {
                page = Utilities.getPage(url); //get the page
            }
            catch (System.Net.WebException wex)
            {
                OnMsg(string.Format("Error al obtener página:{0}", wex.Message));
                OnMsg("Se retorna una página vacía para repetir el proceso");
                page = "";
            }
            catch (Exception ex)
            {
                OnMsg(string.Format("Error indeterminado al obtener la página: {0}", ex.Message));
                page = "";
            }

            rawData = extractRawData(page); //Extract raw data, return a list of text to be reduced.
            if (rawData.Count !=0)
            {
                OnMsg(string.Format("Encontrados {0} datos, que deben procesarse",rawData.Count));
                //To improve: page and data should use only one transaction.
                int idPage = pc.findPage(url);
                //Found page? Create one and also get data.
                if (idPage == -1)
                {
                    pc.addPage(new global::page() { url = url, parsed = false });
                    idPage = pc.findPage(url);
                    List<plate> multas = processRawData(rawData, idPage);
                    plate_ctl.plates = multas;
                    plate_ctl.savePlates();
                    OnMsg(string.Format("{0} registros procesados para la página {1}", multas.Count, url));
                }  else
                {
                    OnMsg(string.Format("Error: la página {0} ya existe en la base de datos!",counter));
                }
            }
            else
            {
                OnMsg(string.Format("La página {0} no tiene resultados...", url));
                OnMsg("Proceso finalizado!");
            }

        } while (rawData.Count!=0);
    }

 /// <summary>
 /// 20170310:Added normalized field for searches.
 /// </summary>
 /// <param name="rawData"></param>
 /// <param name="idPage"></param>
 /// <returns></returns>
        public List<plate> processRawData(List<string> rawData,int idPage)
        {
            List<string> rows = new List<string>();
            List<plate> multas = new List<plate>();
        bool FirstTime = true;
            foreach (string item in rawData)
            {
                rows = new List<string>();
                string fragment = item;
                string remaining = "";
                while (fragment != string.Empty)
                {
                    fragment = Utilities.between(fragment, "<pre>", "</pre>", out remaining);
                    if (fragment != string.Empty)
                    {
                        rows.Add(fragment);
                        fragment = remaining;
                    }
                }
                //If everything went ok, there should be exactly n rows
                plate m = new plate();
                string[] lista = rows.ToArray();
                //Bug in the impo page:if lista is empty trace it but not process it
                if (lista.Length == 0)
                {
                OnMsg("BUG: los datos en la página de la IMPO estan incorrectos, REVISAR!!");
                }
                else

            {
                if (FirstTime)
                {
                    FirstTime = false;
                    continue; //trick to avoid first record (header)
                }
                m.id_page = idPage;
                m.plate_str = lista[0];
                m.date = lista[1];
                m.place = lista[2];
                m.intervention = lista[3];
                m.article = lista[4];
                m.ur = int.Parse( lista[5]);
                m.plate_norm = normalizePlate(lista[0]);
                m.date_norm = normalizeDate(lista[1]);
                multas.Add(m);
                }
            }
        //Supress first data since is the table header.
        return multas;
        }

    /// <summary>
    /// 20170310: Normalizes plate to easier searches
    /// </summary>
    /// <param name="plate"></param>
    /// <returns></returns>
    private string normalizePlate(string plate)
    {
        return plate.Replace(" ", "").Replace("+", "").Replace("-", "").ToUpper(); //remove spaces and usual symbols, convert to uppercase
    }

    /// <summary>
    /// La fecha se recibe en el formato 12/12/2016 11:04
    /// La misma debe convertirse a fecha
    /// </summary>
    /// <param name="fecha"></param>
    /// <returns></returns>
    private DateTime normalizeDate(string fecha)
    {
        DateTime date = new DateTime(int.Parse(fecha.Substring(6, 4)),
                                    int.Parse(fecha.Substring(3, 2)),
                                    int.Parse(fecha.Substring(0, 2)),
                                    int.Parse(fecha.Substring(11, 2)),
                                    int.Parse(fecha.Substring(14, 2)), 0);
        return date;
    }

    /// <summary>
    /// Strategy:find out where is the <TR></TR> elements and then extract data from there.
    /// </summary>
    /// <param name="page"></param>
    public static List<string> extractRawData(string page)
        {
            List<string> rawData = new List<string>();
            string myPage = page;
            string fragment = "initial_value";
            string remaining;
            while (fragment != string.Empty)
            {
                fragment = Utilities.between(myPage, "<TR>", "</TR>", out remaining);
                if (fragment != string.Empty)
                {
                    rawData.Add(fragment);
                    myPage = remaining;
                }
            }
            return rawData;
        }
    /*
      public callImpo(string url)
  {
      //Strategy: every line between <TR> and </TR> is a date
      //so we need to extract them first
      //string url = "http://www.impo.com.uy/bases/notificaciones-cgm/1-2016/1";
      string page = Utilities.getPage(url);
      //Extract raw data, return a list of text to be reduced.
      List<string> rawData = extractRawData(page);

      //Save results to database
      pageCtl pc = new pageCtl(sqlConnection.createConnection());
      plateCtl plate_ctl = new plateCtl(sqlConnection.createConnection());

      //To improve: page and data should use only one transaction.

      int idPage = pc.findPage(url);
      //Found page? Create one and also get data.
      if( idPage==-1)
      {
          pc.addPage(new global::page() { url = url, parsed = false });
          idPage = pc.findPage(url);
          List<plate> multas = processRawData(rawData, idPage);
          plate_ctl.plates = multas;
          plate_ctl.savePlates();
      } else {
        OnMsg(string.Format("No se agregaron datos: la url {0} ya existe en la base de datos", url));
      }

  }
  */
}
