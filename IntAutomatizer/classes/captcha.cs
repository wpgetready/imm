﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// 20170729: Esta clase se ha vuelto obsoleta, puesto que la Intendencia cambió la modalidad de seguridad.
/// Ya no se usan Captchas sino ReCaptcha
/// </summary>
public class captcha
    {
    public int id_captcha;
    public string url;
    public int id_image;
    public string filename;
    public string code;
    public bool valid_code;
    }
