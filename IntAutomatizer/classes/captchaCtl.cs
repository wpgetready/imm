﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// 20170729: Esta clase se ha vuelto obsoleta, puesto que la Intendencia cambió la modalidad de seguridad.
/// Ya no se usan Captchas sino ReCaptcha
/// </summary>
class captchaCtl
    {
        DbConnection currentConn;
        string insertCaptcha = "insert into captcha(url,id_image,filename,code,valid_code) values ('{0}',{1},'{2}','{3}',{4})";
        string findCaptcha_by_id = "Select * from captcha where id_image={0}";
        string updateCaptcha_Qry = "Update captcha set url='{0}',id_image={1},filename='{2}',code='{3}',valid_code={4} where id_captcha={5}";
        string getLastCaptcha_Qry = "select max(id_captcha) from captcha";

        public List<plate> plates = new List<plate>();

        public captchaCtl(DbConnection sqlConn)
        {
            currentConn = sqlConn;
        }

        /// <summary>
        /// Add plate to database
        /// </summary>
        /// <param name="p"></param>
        public int addCaptcha(captcha c )
        {
            try
            {
                using (DbCommand cmd = currentConn.CreateCommand())
                {
                    int flag = (c.valid_code) ? 1 : 0;
                    cmd.CommandText = string.Format(insertCaptcha, c.url, c.id_image, c.filename, c.code,flag);
                    cmd.ExecuteNonQuery();
                    return getLastInsertedCaptcha();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

    /// <summary>
    /// Update captcha in case already exists (unlikely but possible)
    /// </summary>
    /// <param name="c"></param>
    /// <param name="cmd"></param>
    public void updateCaptcha(captcha c)
    {
        try
        {
            using (DbCommand cmd = currentConn.CreateCommand())
            {
                int flag = (c.valid_code) ? 1 : 0;
                cmd.CommandText = string.Format(updateCaptcha_Qry, c.url, c.id_image, c.filename, c.code, flag, c.id_captcha);
                cmd.ExecuteNonQuery();
            }
        }
        catch (Exception)
        {
            throw;
        }
    }


    /// <summary>
    /// find captcha by id, return captcha_id or -1 if not found.
    /// </summary>
    /// <param name="id_captcha"></param>
    /// <returns></returns>
    public int getLastInsertedCaptcha()
        {
            try
            {
                using (DbCommand cmd = currentConn.CreateCommand())
                {
                    cmd.CommandText = getLastCaptcha_Qry;
                    object obj = cmd.ExecuteScalar();
                    int result = -1;
                    if (obj != null)
                    {
                        result = int.Parse(obj.ToString());
                    }
                    return result; //WARNING: possible return incorrectly results
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

}
