﻿using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Data.Common;
using System.Windows.Forms;
using System.Threading;
using System.Drawing.Imaging;

/*
public delegate void consultaEventHandler(object sender, string msg);
public delegate void imageEventHandler(object sender, imgData data);

public struct imgData
{
    public int imageNumber;
    public string picturePath;
}
*/
/// <summary>
/// This class attempt to call the form and provide data to pass through
/// The form call to http://www.montevideo.gub.uy/consultainfracciones
/// and tries to provide the needed information to validate the information
/// TODO: This class needs to be simplified!
/// </summary>
public class consultaInfracciones
    {
    /// <summary>
    /// Handler for events, for message decoupling
    /// I make two events, one to send messaging, another used to display pictures.
    /// For command line imageEventHandler won't be used, but it will for windows forms.
    /// </summary>
    /// 

    #region Event Handling
    //This code handles all events related to this class.
    //Notice some events won't be handled on command line execution
    public event consultaEventHandler Msg;
    public event imageEventHandler Picture; //only for windows forms.

    protected virtual void OnMsg(string msg)
    {
        if (Msg!=null){Msg(this, msg);}
    }

    protected virtual void OnPicture(imgData data)
    {
        if (Picture != null){Picture(this, data);}
    }
    #endregion

    const string url_infracciones = "http://www.montevideo.gub.uy/consultainfracciones";
    const string url_sitio_web = "http://www.montevideo.gub.uy";

    string captcha_sid;
    string captcha_token;
    string form_build_id;
    string content;
    string response_filename="response_general.htm"; //filename to save response, it will change for every test.
    captcha current_captcha; //store current captcha_id for updating.
    static DbConnection currConn;
    public string statusMsg; //Error message to return in case of failing

    ///Contadores de control
    int plate_Counter;
    double captchaOK_Counter;
    double captchaWrong_Counter;
    int imgCounter;


    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="dbc"></param>
    public consultaInfracciones()
    {
        currConn = sqlConnection.createConnection();
    }
    /// <summary>
    /// Main method
    /// TODO: Control EVERYTHING in case of error.
    /// 20172907: La página de la Intendencia cambió hace meses y me di por vencido
    /// Sin embargo pasaron 2 cosas trascendentes:
    /// 1-NO cerré la aplicación, la cual siguió creciendo
    /// 2-Volví a la carga, para encontrar que aunque está mas difícil, SI es posible obtener las imagenes.
    /// 3-Alguien dijo fácil? Es difícil, pero no es imposible.
    /// 4-So, volvemos a la carga nuevamente.
    /// </summary>
    public void Start()
    {
        DateTime start = DateTime.Now;

        string fullPath = "";
        bool captchaSuccess = false;
        CaptchaSniper ics = new infraccionesCaptchaSniper(); //Initialize CaptchaSniper
        captcha c = null;
        plateCtl matriculas = new plateCtl(currConn);
        matriculas.loadPlates(false); //load plates not processed
        Random rnd = new Random();
        foreach (plate matricula in matriculas.plates)
        {
            do
            {
                captchaSuccess = false;
                OnMsg("--------------------Invocando la página de la Intendencia, wait---------------------");
                c = callPage(); //Call the page, return captcha to solve.
                if (c==null) //if there was an error attempting to get the page...
                {
                    OnMsg("Hubo un error al invocar la página, esperamos 5 segundos y volvemos a intentar.");
                    Thread.Sleep(1000 * 5);  //Wait for 5 seconds...
                    continue; //and try again
                }
                fullPath = Path.Combine(Utilities.BASE_DIR_CAPTCHA, c.filename);

                OnMsg(string.Format("captcha path guardado en {0}",  fullPath));
                CSResult result = ics.GetCaptchaSolved(fullPath, true);
                //CSResult need to return code 200, saying everything went ok.
                if (result.ResultCode!="200")
                {
                    //There were an error, process it somehow.
                    OnMsg(string.Format("Error al invocar Captcha: resultcode = {0} (deberia ser 200)", result.ResultCode));
                    OnMsg("Abortando el proceso...");
                    return;
                }
                OnMsg(string.Format("Respuesta recibida desde Captcha Sniper: {0}",result.Text));

                if (ics.isValidAnswer(result.Text))
                {
                    OnMsg("Se considera la respuesta como aceptable, probamos acceder...");
                    captchaSuccess = sendData(matricula, result.Text); //Send data to process, prepare it.
                } else
                {
                    OnMsg("La respuesta no válida,saving captcha and answer...");
                    updateCaptcha(result.Text, false);
                }
                OnMsg("Espero entre 0 y 5 segundos para continuar...");
                Thread.Sleep(1000 * rnd.Next(0, 5));
                OnMsg("----Stats----");
                OnMsg(string.Format("Tiempo transcurrido: {0}, Imagenes guardadas: {1}", DateTime.Now-start, imgCounter));
                OnMsg(string.Format("Matriculas procesadas:{0}, total a procesar:{1}, restantes: {2}", plate_Counter, matriculas.plates.Count, matriculas.plates.Count - plate_Counter));
                OnMsg(string.Format("Captchas resueltos:{0},erroneos:{1},total:{2},ratio de exito:{3:0.0%}",captchaOK_Counter,captchaWrong_Counter,
                    captchaOK_Counter+captchaWrong_Counter, (captchaOK_Counter/(captchaWrong_Counter+ captchaOK_Counter+1))
                 ));

            } while (!captchaSuccess); //repeat process if we didn't have a captcha success
            plate_Counter++;
        }
        OnMsg("Proceso terminado, todas las matrículas han sido procesadas");
    }

    /// <summary>
    /// This method will call de infraction page, saving the captcha for further processing.
    /// Notice that we don't have enough information to process anything. We'll scan the page to track information to resend later
    /// </summary>
    /// <returns>the path where the captcha is stored.</returns>
    private captcha callPage()
    {
        captcha c = new captcha() { valid_code = false, code="" };

        try
        {
            content = Utilities.getPage(consultaInfracciones.url_infracciones); //get Page data, return content
        }
        catch (System.Net.WebException wex)
        {
            OnMsg(string.Format("Error al obtener página:{0}", wex.Message));
            return null;
        }
        catch (Exception ex)
        {
            OnMsg(string.Format("Error indeterminado al obtener la página: {0}", ex.Message));
            return null;
        }
        

        //Get special data, we will need it to send it again
        captcha_sid = extract_value("captcha_sid", content); //Extract information about captcha. get sid, data need for identificacion
        captcha_token = extract_value("captcha_token", content); //get token

        //form_build_id is repeated at least three times in the page (!) So we need to find a reference first, and THEN look for the real we want to fetch.
        string reference = content.Substring(content.IndexOf("consulta-multas-form")); //this is our helper....

        form_build_id = extract_value("form_build_id", reference); //form id
        
        c.url = extract_value("foaf:Image", content, "src"); //Right, now we need the captcha image
        c.filename = "captcha_" + captcha_sid + ".jpg";
        Utilities.DownloadRemoteImageFile(url_sitio_web + c.url, c.filename, Utilities.BASE_DIR_CAPTCHA);
        //Save captcha information on database.
        current_captcha=addCaptchatoDB(c); //Add captcha, return captcha_id created
        return c;
    }

    /// <summary>
    /// Overloaded method used from command line.
    /// </summary>
    /// <param name="matricula"></param>
    /// <param name="captchaAnswer"></param>
    /// <returns></returns>
    private bool sendData(plate matricula, string captchaAnswer)
    {
        return sendData(matricula.plate_str, matricula.date, captchaAnswer, matricula.id_plate);
    }

    /// <summary>
    /// Mimics sending form to site, with the proper information
    /// to sendData, previously we called callPage
    /// </summary>
    public bool sendData(string matricula,string fecha,string captcha_answer,int id_plate)
    {
        string responseString = "";
        //Step 1: Encode data to be sent
        //Base format:
        //matricula =SAO+8912&fecha_multa%5Bdate%5D=10%2F12%2F2016&captcha_sid=504356&captcha_token=4600d12bce265f863f7e84dd05c730f2&captcha_response=8ZBTA&op=Consultar&form_build_id=form-AIdpXSokIPw77w3YVhr-DLIUO65VePyWbXXGcqNYWFU&form_id=consulta_multas_form
        string pD = "matricula={0}&fecha_multa%5Bdate%5D={1}&captcha_sid={2}&captcha_token={3}&captcha_response={4}&op=Consultar&form_build_id={5}&form_id=consulta_multas_form";

        //IMPORTANT: meaning of different places
        //few notes: 0 - plate: replace ' ' with '+'
        //           1 - date in the format dd/mm/yyyy uri encoded. NO TIME, NO SECONDS!!!!
        //           2 - captcha_sid
        //           3 - captcha_token
        //           4 - captcha_response = value of captcha expected (Currently manually solved)
        //           5 - form_build_id

        string mat = matricula.Replace(' ', '+'); //Replace space by '+' in plates
        //Encode date see http://stackoverflow.com/questions/575440/url-encoding-using-c-sharp/7427556#7427556
        string fecha_cut = WebUtility.UrlEncode(fecha.Substring(0, 10));
        string msg = string.Format(pD, mat, fecha_cut, captcha_sid, captcha_token, captcha_answer, form_build_id);
        //Step 2: send the data.
        OnMsg(string.Format("Enviando matricula:{0},fecha={1},valor captcha={2}", matricula, fecha, captcha_answer));
        try
        {
            responseString = Utilities.postCall(msg, url_infracciones);//Send the encoded data, wait for the answer
        }
        catch (System.Net.WebException wex)
        {
            OnMsg("Excepción en PostCall al invocar la página de infracciones. Lo procesamos como un fallo para que se repita todo");
            OnMsg(string.Format("Detalles del error: {0}" , wex.Message));
            return false;
        }
        catch (Exception ex)
        {

            OnMsg("Error general en PostCall al invocar la página de infracciones. Repetimos el procedimiento");
            OnMsg(string.Format("Detalles del error: {0}", ex.Message));
        }
        
        response_filename = "response_" + captcha_sid + "_" + captcha_answer + ".htm";         
        //Step 3(optional): save page returned to disk for later analysis
        Utilities.writeAllFile(Path.Combine(Utilities.BASE_DIR_RESPONSES,response_filename), responseString); //Save response for analysis, just in case
        OnMsg("Salvando respuesta htm en:");
        OnMsg(response_filename);
        //Step 4: process response page.
        return processResponse(responseString,matricula,id_plate,captcha_answer,fecha); //processResponse, to see how it went.
    }

/// <summary>
/// Process response, depending on the results we do one thing or another.
/// </summary>
/// <param name="responseString"></param>
/// <param name="matricula"></param>
/// <param name="id_plate"></param>
/// <param name="captcha_answer"></param>
    public bool processResponse(string responseString,string matricula,int id_plate,string captcha_answer,string fecha)
    {
        statusMsg = "";//reset status msg
        string mat = matricula.Replace(' ', '_');
      
        if (encodedImageExists(responseString))
        {
            OnMsg("EXITO!: procesando imagenes...");
            //Case 1:If we got here, we suceded
            saveImagesJpg(responseString,mat,id_plate,fecha); //-Save the image(s) related (inside saveImages) and also update database
            //Mark the plate as correctly processed.
            OnMsg(string.Format("marcando matricula ID {0} como exitosa" ,id_plate));
            markPlateAsProcessed(id_plate, "Processed OK!");
            OnMsg(string.Format("Actualizando captcha obtenido con la respuesta {0} como correcto",captcha_answer));
            updateCaptcha(captcha_answer, true); //save data always, whenever it has succeed or not, flagging properly.
            return true;
        }
        OnMsg("No tuvimos éxito, determinando la razón...");
        //If not Image detected, two chances: Captcha error or simply there is no images (some kind of error here)
        bool CaptchaSuccess = false;
        //We didn't have images.
        //Two chances: captcha error or some problem related (there are plates without images also)
        //Example: 4678 JGV has not image records
        //In this case we need to move forward, the process was valid, but not images were found.
        //Also, a reason need to be stored with the images to explain what happened. 
        if (!checkError(responseString))
        {
            //We didn't have an error. Could it be possible there is another reason
            string reason;
            if (checkReason(responseString,out reason)) //Check if there is a reason, return in the reason parameter
            {
                OnMsg(string.Format("La página retorna la siguiente razón:{0}",reason));
                OnMsg(string.Format("Asi que despues de todo, el proceso salió bien! Se marca la matricula ID {0} como procesada OK", id_plate));
                //Yes there was a problem getting the images, that wasn't our fault, is page fault!
                CaptchaSuccess = true; //so the captcha was ok then!
                //we also have to mark the image as processed.
                markPlateAsProcessed(id_plate,reason);
            }
        } else
        {
            OnMsg(string.Format("No hubo suerte, hay que probar de nuevo, la razón es: {0}", statusMsg));
        }

        updateCaptcha(captcha_answer, CaptchaSuccess); //save data always, whenever it has succeed or not, flagging properly.
        return CaptchaSuccess;
        //What else is needed? Continue polishing error system. Make some more tests.
    }

    /// <summary>
    /// 20170806: Adapted, however is obsolete.
    /// </summary>
    /// <param name="id_plate"></param>
    private void markPlateAsProcessed(int id_plate,Utilities.processResult pResult, string reason)
    {
        plateCtl pc = new plateCtl(currConn);
        pc.updatePlate(id_plate, pResult, reason);
    }

    /// <summary>
    /// Extract error if possible
    /// What we know: error is sourrounded with <div class="messages--error messages error">....</div>
    /// So we'll try to isolate the problem
    /// </summary>
    /// <param name="response"></param>
    private bool checkReason(string response, out string why)
    {
        why = "";
        string fragment;
        string reason = Utilities.between(response, "replace_textfield_div", "</div>", out fragment).Trim();
        reason = reason.Replace("\n", "");
        reason = reason.Replace("\t", "");
        reason = reason.Replace(">", "");
        reason = reason.Replace("'", "");
        reason = reason.Replace("\"", "");
        if (reason.Length!=0)
        {
            statusMsg = reason;
            why = reason;
            return true; ;
        }
        if (response_filename!="")
        {
            statusMsg = string.Format("Hubo un problema indeterminado para obtener los datos, verificar el archivo {0}", response_filename);
            return false;
        }
        statusMsg = string.Format("Hubo un problema indeterminado para obtener los datos y tampoco pude grabar el archivo. Esto es un desastre...");
        return false;
    }

    /// <summary>
    /// Scan for errors, return true if error,otherwise false
    /// STORE error message on statusMsg variable
    /// </summary>
    /// <param name="response"></param>
    /// <returns></returns>
    private bool checkError(string response)
    {
        string fragment;
        string msgError = Utilities.between(response, "messages--error messages error", "</div>", out fragment);
        if (fragment == "") return false; //if fragment empty then MAYBE is not an erro
        if (msgError.Length < 2) return false;
        statusMsg = msgError.Substring(2); //skip two first characters (">)
        statusMsg = Regex.Replace(statusMsg, "<.*?>", string.Empty); //Strip all HTML tags
        return true;
    }



    /// <summary>
    /// Try to images if possible.
    /// I saw between 1 to 2 images in general. The process is thought to extract all images, no matter how many.
    /// IMPORTANT: I won't save the images on database since db is constantly growing.
    /// In worst case scenario, I could save only retrieved plates, to caching the images for faster retrieval
    /// That would be in the second version
    /// 2017011: Improved. There is an issue that I'd never thought before: what if a person has multiple tickets in one day?.
    /// Or what if a person has multiple ticket on several days? In short, date is important to discriminate the file.
    /// TODO: images has to be converted to JPG 50% before saving it to the database.
    /// </summary>
    /// <param name="response"></param>
    private void saveImagesPng(string response,string mat,int id_plate,string fecha)
    {
        string detect_image = @"<img\ssrc='data://image/png;base64,(?<base64>.*?)'";
        Match match = Regex.Match(response, detect_image);
        int counter = 0;
        string img_name;
        string img_base_64;
        while (match.Success)
        {
            img_base_64 = match.Groups["base64"].Value;
            img_name = @"img_"   + mat.Replace(' ','_') + "_" 
                       + Utilities.reversedDate(fecha) + "_"  + counter + ".png";  
            savePng(img_base_64, img_name);
            saveImgDB(img_name, id_plate);  //save data in database
            //Raise an event to display the image 
            OnMsg(string.Format("Guardando imagen {0} en {1}", counter, img_name));
            OnPicture(new imgData() { imageNumber = counter, picturePath = Path.Combine(Utilities.BASE_DIR_IMAGES, img_name) });
            match =match.NextMatch(); //go next match
            counter++; //use a counter to have different images filenames
        }
    }

    private void saveImagesJpg(string response, string mat, int id_plate, string fecha)
    {
        string detect_image = @"<img\ssrc='data://image/png;base64,(?<base64>.*?)'";
        Match match = Regex.Match(response, detect_image);
        int counter = 0;
        string img_name;
        string img_base_64;
        while (match.Success)
        {
            img_base_64 = match.Groups["base64"].Value;
            img_name = @"img_" + mat.Replace(' ', '_') + "_"
                                        + Utilities.reversedDate(fecha) + "_"
                                        + counter + ".jpg";
            saveJpg(img_base_64, img_name, 50);
            saveImgDB(img_name, id_plate);  //save data in database
            //Raise an event to display the image 
            OnMsg(string.Format("Guardando imagen {0} en {1}", counter, img_name));
            OnPicture(new imgData() { imageNumber = counter, picturePath = Path.Combine(Utilities.BASE_DIR_IMAGES, img_name) });
            match = match.NextMatch(); //go next match
            counter++; //use a counter to have different images filenames
        }
    }

    /// <summary>
    /// Saves data to database
    /// WARNING: currently assuming that every images will be different.
    /// I mean I'M NOT CONSIDERING CASE WHEN I HAVE REPEATEAD plates for examples. 
    /// This need to be analyzed.
    /// </summary>
    /// <param name="img_name"></param>
    /// <param name="id_plate"></param>
    private void saveImgDB(string img_name,int id_plate)
    {
        photo p = new photo() { filename = img_name, id_plate = id_plate };
        photoCtl pc = new photoCtl(currConn);
        pc.addPhoto(p);
    }

    /// <summary>
    /// Save base64 image as file
    /// Discussion: http://stackoverflow.com/questions/5400173/converting-a-base-64-string-to-an-image-and-saving-it
    /// HOWEVER: make image.Save INSIDE , otherwise there will be an error.
    /// </summary>
    /// <param name="base64String"></param>
    /// <param name="path"></param>
    private void savePng(string base64String, string path)
    {
        byte[] bytes = Convert.FromBase64String(base64String);
        string fullPath = Path.Combine(Utilities.BASE_DIR_IMAGES, path);
        Image image;
        MemoryStream ms = new MemoryStream(bytes);
        image = Image.FromStream(ms);
        image.Save(fullPath, System.Drawing.Imaging.ImageFormat.Png);
        ms.Close();
     }

    /// <summary>
    /// Save base64 image as jpeg file
    /// Discussion: https://msdn.microsoft.com/en-us/library/bb882583(v=vs.110).aspx
    /// </summary>
    /// <param name="base64String"></param>
    /// <param name="path"></param>
    /// <param name="Quality"></param>
    private void saveJpg(string base64String, string path, byte Quality)
    {
        imgCounter++;

        byte[] bytes = Convert.FromBase64String(base64String);
        string fullPath = Path.Combine(Utilities.BASE_DIR_IMAGES, path);
        Image image;
        MemoryStream ms = new MemoryStream(bytes);
        image = Image.FromStream(ms);
        Encoder myEncoder = Encoder.Quality;
        EncoderParameters myEncoderParameters = new EncoderParameters(1);
        EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, Quality);
        myEncoderParameters.Param[0] = myEncoderParameter;
        image.Save(fullPath, System.Drawing.Imaging.ImageFormat.Jpeg);
        ms.Close();
    }

    /// <summary>
    /// Checks if the page has an encoded image
    /// In fact this is a little redundant and it should be improved.
    /// </summary>
    /// <param name="response"></param>
    /// <returns></returns>
    private bool encodedImageExists(string response)
    {
        string detect_image = @"<img\ssrc='data://image/png;base64,(.*?)'";
        Match match = Regex.Match(response, detect_image);
        return match.Success;
    }
    
    /// <summary>
    /// extract the value after some attribute
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    private string extract_value(string name,string content)
        {
            return extract_value(name, content, "value");
        }

    /// <summary>
    /// Extract a value from a content, scanning a tag inside it.
    /// </summary>
    /// <param name="name"></param>
    /// <param name="content"></param>
    /// <param name="tag"></param>
    /// <returns></returns>
    private string extract_value(string name, string content, string tag)
        {
            string temp;
            try
            {
                temp = content.Substring(content.IndexOf(name));
                if (temp == "") return string.Empty;
                temp = temp.Substring(temp.IndexOf( tag + "=" ) + tag.Length + 2); //2 refers the " and 1 more character to properly position.
                if (temp == "") return string.Empty;
                temp = temp.Substring(0, temp.IndexOf('"'));
                if (temp == "") return string.Empty;
                temp = temp.Replace("\"", "");
                return temp;
            }
            catch (Exception ex)
            {

                Console.WriteLine("ooops... Error when extracting value {0} from content! Error: {1}", name, ex.Message);
                return string.Empty;
            }
        }

    /// <summary>
    /// Save captcha details on database
    /// </summary>
    private captcha addCaptchatoDB(captcha c)
    {
        captchaCtl cc = new captchaCtl(currConn);
        c.id_captcha = cc.addCaptcha(c);
        return c;
    }

    /// <summary>
    /// updates current captcha data into database
    /// </summary>
    /// <param name="captcha_answer"></param>
    /// <param name="is_valid"></param>
    private void updateCaptcha(string captcha_answer, bool is_valid)
    {
        if (!is_valid)
        {
            captchaWrong_Counter++;
        }
        else
        {
            captchaOK_Counter++;
        }

        if (current_captcha != null)
        {
            current_captcha.valid_code = is_valid;
            current_captcha.code = captcha_answer;
            captchaCtl cc = new captchaCtl(currConn);
            cc.updateCaptcha(current_captcha);
        }
    }
    //Adds captcha
    private void createCaptcha(string captcha_answer, bool is_valid)
    {
        if (current_captcha != null)
        {
            current_captcha.valid_code = is_valid;
            current_captcha.code = captcha_answer;
            captchaCtl cc = new captchaCtl(currConn);
            cc.addCaptcha(current_captcha);
        }
    }
}

/*Typical response

POST /consultainfracciones HTTP/1.1
Host: www.montevideo.gub.uy
Connection: keep-alive
Content-Length: 249
Cache-Control: max-age=0
Origin: http://www.montevideo.gub.uy
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36
Content-Type: application/x-www-form-urlencoded
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,/;q=0.8       <-----WATCHOUT THIS 
Referer: http://www.montevideo.gub.uy/consultainfracciones
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.8,es-419;q=0.6,es;q=0.4
Cookie: _gat=1; has_js=1; _ga=GA1.3.429929681.1483654915
matricula=SAO+8912&fecha_multa%5Bdate%5D=10%2F12%2F2016&captcha_sid=504356&captcha_token=4600d12bce265f863f7e84dd05c730f2&captcha_response=8ZBTA&op=Consultar&form_build_id=form-AIdpXSokIPw77w3YVhr-DLIUO65VePyWbXXGcqNYWFU&form_id=consulta_multas_form

 
 IN WATCH OUT THIS ther is a ,/; but I can't reproduce the REAL data which is * / * these 3 characters but together, withou spaces
 * 
image is in the form
 * http://www.montevideo.gub.uy/image_captcha?sid=504466&amp;ts=1483657929
 * Me lo imaginé: si se hace la consultas dias despues este sid no funciona(!)
 */

//save picture in a captcha cache (I found the cache use a reference number. There is a chance in a million to be repeated,
//References:
//http://stackoverflow.com/questions/11511511/how-to-save-a-png-image-server-side-from-a-base64-data-string
//http://stackoverflow.com/questions/18827081/c-sharp-base64-string-to-jpeg-image
//http://stackoverflow.com/questions/5400173/converting-a-base-64-string-to-an-image-and-saving-it

//but still is a chance. Is it useful to reuse it? Don'know yet, but in 6 iterations we already have around 2000 querys to be made,
//and also with a high chance of failure, so it could be about 4000 tries, whici is about 1/250 to repeat the captcha image.
