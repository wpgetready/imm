﻿using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Data.Common;
using System.Threading;
using System.Drawing.Imaging;
using IntAutomatizer.classes.Anticaptcha;
using Newtonsoft.Json;

public delegate void consultaEventHandler(object sender, string msg);
public delegate void imageEventHandler(object sender, imgData data);

public struct imgData
{
    public int imageNumber;
    public string picturePath;
}


/// <summary>
/// Call the form http://www.montevideo.gub.uy/consultainfracciones providing the needed information to validate
/// </summary>
public class consultaInfraccionesV2
    {
        /// <summary>
        /// Handlers for events & message decoupling
        /// </summary>
        /// 
        public const string url_infracciones = "http://www.montevideo.gub.uy/consultainfracciones";
        public const string url_sitio_web = "www.montevideo.gub.uy"; //used in call ajax invoke watchout how it is spelled!
        public const string url_ajax = "http://www.montevideo.gub.uy/system/ajax"; //Ajax call for retrieveing images

    #region Event Handling
    //This code handles all events related to this class. Some events won't be handled on command line execution
    public event consultaEventHandler Msg;
    public event imageEventHandler Picture; //only for windows forms.

    protected virtual void OnMsg(string msg) { if (Msg!=null){Msg(this, msg);}}
    protected virtual void OnPicture(imgData data){if (Picture != null){Picture(this, data);}}

    #endregion

    private string last_saved_file; //tracking las file save, used to inform user

    //Class-variables extracted from page values
    string captcha_sid;
    string captcha_token;
    string form_build_id;


    static DbConnection currConn;
    public string statusMsg; //Error message to return in case of failing

    public consultaInfraccionesV2()
    {

        currConn = sqlConnection.createConnection();         //For some unknown reason, app refuses of create connection (?)
        //currConn = null;
    }
    /// <summary>
    /// Start, starts process from database, collecting all plates without being processed.
    /// </summary>
    public void Start()
    {
        int plate_Counter = 0;
        DateTime start = DateTime.Now;
        string page_content;
        bool processOK = false;
        plateCtl matriculas = new plateCtl(currConn);
        matriculas.loadPlates(false); //load plates not processed
        Random rnd = new Random();
        AntiCaptcha ac= new AntiCaptcha();
        ACGetTaskResult result = null;
        string jsonData = "";
        int rc_aciertos = 0;
        int rc_errores = 0;
        int sumTime = 0;


        foreach (plate matricula in matriculas.plates)
        {
            do
            {
                processOK = false;
                try
                {
                    result = ac.invokeAntiCaptcha(); //Get recaptcha result, it can take time(seconds/minutes).This needs to be made first (for timely reasons)
       
                }
                catch (System.NullReferenceException nre)
                {
                    //Este error ha sucedido en una ocasión
                    Utilities.Log("Error al invocar Anticaptcha, esperar dos segundos y reintento....");
                    Thread.Sleep((1000 * 5)); //Espero dos segundos y vuelvo a intentar
                    continue;
                }


                if (result == null)
                {
                    Utilities.Log("Error Anticaptcha, esperar dos segundos y reintento....");
                    Thread.Sleep((1000*2)); //Espero dos segundos y vuelvo a intentar
                    continue;
                }

             //el parametro que se obtiene se pasa como dato hacia sendData
                OnMsg("--------------------Invocando la página de la Intendencia, wait---------------------");
                page_content = extractContentFromPage(url_infracciones, matricula); //return content page or null if error.
                if (page_content==null) //if there was an error (null)
                {
                    //This case can be due because there are not workers to solve the recaptcha (since I have seen it)
                    OnMsg("Hubo un error al invocar la página, esperamos 5 segundos y volvemos a intentar.");
                    Thread.Sleep(1000 * 5);  //Waitand try again
                    continue; 
                }
                //If we got here, we have 1) recaptcha answer and 2)page content (even we don't know if content is correct)
                //Let's combine both to see if everything worked out...
               OnMsg("Ensamblando respuesta e invocando pagina...");
               string response = sendData(matricula, result.solution.gRecaptchaResponse); //Send data, wait for answer
                if (response == null) //There was an error: server down, Internet down, everything else.
                {
                    OnMsg("Esperamos unos segundos antes de reintentar");
                    Thread.Sleep(1000 * rnd.Next(0, 5));
                    continue;
                }
                if (response.Contains("id='datos_multa'")) //Check if we could access
                {
                    rc_aciertos++; //Yes, let's continue
                    //1-Tuvimos éxito y llegamos a la página, invocar Ajax para obtener las imagenes
                    OnMsg(string.Format("Obtengo página de matricula {0}",matricula.plate_norm));
                    jsonData=ajaxCall(response,matricula); //Time to call Ajax . Return a json when back.
                    extractImages(jsonData,matricula); //Try to get images from json if possible.
                    markPlateAsProcessed(matricula.id_plate, Utilities.processResult.Success, "Processed OK!"); //mark the plate as successful
                    processOK = true; //continue with next plate
                }
                else
                {
                    //TODO: there is one case that is NOT an error but is counted as one.
                    //Modify handleError to make a new one name  isThisanError, and return if it is or not an error
                    //to incremente the variables properly and process it in such cases.
                    if (isAnError(response, matricula))                     //Returns true it is an error or false if not.
                    {
                        rc_errores++;
                        continue;
                    }
                    else //if False, statusMsg contains errors details
                    {
                        rc_aciertos++;  //We got a success case, but no pictures.
                        markPlateAsProcessed(matricula.id_plate,Utilities.processResult.SuccessNoImages, statusMsg); //mark the plate as successful, without pictures
                        processOK = true; //continue with next plate
                        OnMsg(string.Format("DETECTADO: NO PICTURES :{0}: {1}", matricula.plate_norm, statusMsg));
                    }
                }
                sumTime += ac.secondsElapsed;
                OnMsg("----Stats----");
                OnMsg(string.Format("Tiempo transcurrido: {0}, Aciertos: {1}, Errores {2}", DateTime.Now-start, rc_aciertos,rc_errores));
                OnMsg(string.Format("Tiempo promedio de invocaciones {0}, tiempo promedio de casos exitosos:{1}", (sumTime/(rc_aciertos+rc_errores)), sumTime / (rc_aciertos+1)));
                OnMsg(string.Format("Matriculas procesadas:{0}, total a procesar:{1}, restantes: {2}", plate_Counter, matriculas.plates.Count, matriculas.plates.Count - plate_Counter));
            } while (!processOK); //repeat process if we didn't have a captcha success
            plate_Counter++;
        }
        OnMsg("Proceso terminado, todas las matrículas han sido procesadas");
    }

    /// <summary>
    /// handle all kind of error in the process
    /// </summary>
    /// <param name="response"></param>
    /// <param name="matricula"></param>
    /// <returns></returns>
        private bool isAnError(string response, plate matricula)
        {
            if (!checkError(response, "messages--error messages error", "</div>")) //true if the page contains an error, details are stored on statusMsg variable
            {
                //Something went bad and I don't know what it is.
                OnMsg(string.Format("ERROR INESPECIFICO con la matricula {0},chequear {1}", matricula.plate_norm,last_saved_file));
                OnMsg("Espero 2 segundos para continuar...");
                Thread.Sleep(2000);
                return true;
            }
            //Tenemos un mensaje de error,veamos si tenemos el error HTTP_USER_AGENT para ignorarlo.
            if (!statusMsg.Contains("HTTP_USER_AGENT"))
            {
            //No special case, probably we screwed it ...
                OnMsg(string.Format("ERROR NINGUN ERROR ESPECIFICO {0} :  {1}", matricula.plate_norm, statusMsg));
                OnMsg("Espero 2 segundos para continuar...");
                Thread.Sleep(2000);
                return true;
            }
            //Contiene HTTP_USER_AGENT, verifico que se nos devuelva un mensaje de que no se generaron imagenes.Busco el mensaje
            if (!checkError(response, "replace_textfield_div", "</div>")) //true if there is an error result remain in statusMsg
            {
            //No encontré el mensaje, algun problema diferente hubo.
                OnMsg(string.Format("ERROR SE DETECTO HTTP_USER_AGENT PERO NO UN ERROR ESPECIFICO PARA LA MATRICULA {0} :  {1}", matricula.plate_norm, statusMsg));
                OnMsg("Espero 2 segundos para continuar...");
                Thread.Sleep(2000);
                return true;
            }
            //Verifico si el mensaje es del tipo "No se generaron imagenes ...."
            if (!statusMsg.Contains("No se generaron")) 
            {
                //Estamos ante un caso diferente, donde tenemos un mensaje de error nuevo.
                OnMsg(string.Format("ERROR SE DETECTO HTTP_USER_AGENT Y UN ERROR INDETERMINADO PARA LA MATRICULA {0} :  {1}", matricula.plate_norm, statusMsg));
                OnMsg("Espero 2 segundos para continuar...");
                Thread.Sleep(2000);
                return true;
        }
            return false; //este  no es un caso de error, es un caso donde no tenemos imagenes y es un caso de exito.
        }

        /// <summary>
    /// extract images from jsonData. All is explained in the code
    /// </summary>
    /// <param name="jsonData"></param>
        private void extractImages(string jsonData, plate matricula)
    {
        int counter = 0;
        //jsonData is in the format described on jsonResponse class, data is returned as an array
         jsonResponse[] jr = JsonConvert.DeserializeObject<jsonResponse[]>(jsonData);
        //Currently we could have a maximum of two images per data (or I guess so), so wee need a counter to tell them apart
        foreach (jsonResponse r in jr)
        {//if insert command, image needs to be extracted from data (when is not empty)
            if (r.command == "insert" && r.data !="")
                {
                        saveImagesJpg(r.data, matricula,counter, Utilities.BASE_DIR_IMAGES,true);
                        counter++;
                }
        }
    }

    /// <summary>
    /// 
    /// Simula una invocación de Ajax para obtener json data con la información de las imagenes
    /// 20170801: he investigado y encuentro que UNA sola invocación trae la información de TODAS las imágenes. El 99% de las veces trae 1 o tal vez 2 , por eso 
    /// no se molestaron en depurar esta rutina. Lo que importa es que ganaron ancho de banda al exigir que esto se haga con el click de un botón por mas que por cada
    /// call llame a dos imagenes.
    /// El proceso es en extremo parecido a la invocación con recaptcha, con sus obvias variaciones.
    /// Version 1:no agrego AJAX info porque es grande y repetitiva, veremos si se puede hacer esta omision
    /// Version 2: no es necesaria la información como había pensado inicialmente

    /// </summary>
    /// <param name="response"></param>
    /// <returns>la respuest en supuesto format JSON. </returns>
    public string ajaxCall(string response, plate matricula)
        {
            //form_build_id=form-fr0PBaqkWoVvbUOfCZ2_wYCnKN8q8sVwA2QrEgmBcFo&form_id=consulta_multas_form&captcha_sid=1385310&captcha_token=0d647a36d641bfdd826d495cd5428622
            string pD ="form_build_id={0}&form_id=consulta_multas_form&captcha_sid={1}&captcha_token={2}";
            OnMsg(("Simulando invocación Ajax..."));
            
            extractValues(response); //refresh captcha_sid,captcha_token and form_build_id vars

            string msg = string.Format(pD, form_build_id, captcha_sid,  captcha_token);
            OnMsg(string.Format("Enviando msg:{0}",msg));

            string jsonData = Utilities.postCallAjax(msg, url_ajax); //Invoke server and get jsonResponse
            //save jsonData for further analysis (in case of errors,etc.)
            string content_filename = "response_json_" + matricula.plate_norm + "_" + matricula.id_plate + ".htm";
            OnMsg(string.Format("Salvando resultados en: {0}", content_filename));
            Utilities.writeAllFile(Path.Combine(Utilities.BASE_DIR_RESPONSES, content_filename), jsonData);
            last_saved_file = content_filename;
            return jsonData;
        }

    /// <summary>proc
    /// Extracts relevant information from the page, leaving data in several variables.
    /// </summary>
    /// <param name="response"></param>
        private void extractValues(string response)
        {
            captcha_sid = extract_value("captcha_sid",response); 
            captcha_token = extract_value("captcha_token", response); 
            //Look for a reference BEFORE getting form_build_id (form_build_id can be in several places over the content)
            string reference = response.Substring(response.IndexOf("consulta-multas-form")); 
            form_build_id = extract_value("form_build_id", reference); //form id
        }

        /// <summary>
    /// Call the page, extract data from it to be processed.
    /// This is a complete new version , with few more parameters
    /// 
    /// </summary>
    /// <param name="url"></param>
    /// <returns>the path where the captcha is stored.</returns>
    public string extractContentFromPage(string url, plate matricula)
        {
            string content = "";
        /*
         * form_build_id= varía por formulario-->CAPTURE using extract_value
         * captcha_sid = varia por formulario -->CAPTURE using extract_value
         *captcha_token = varia por formulario-->CAPTURE using extract_value
         * Todos los parametros que no se capturan se agregan posteriormente
        */
        try
        {
            content = Utilities.getPage(url); //get Page data, return content
        }
        catch (System.Net.WebException wex)
        {
            OnMsg(string.Format("Error al obtener página:{0}", wex.Message));
            return null;
        }
        catch (Exception ex)
        {
            OnMsg(string.Format("Error indeterminado al obtener la página: {0}", ex.Message));
            return null;
        }
        
        extractValues(content); //refresh captcha_sid,captcha_token and form_build_id vars

        //Save content in file for further analysis.
        string content_filename = "response_" + matricula.plate_norm + "_" + matricula.id_plate  + ".htm";
        Utilities.writeAllFile(Path.Combine(Utilities.BASE_DIR_RESPONSES, content_filename), content);
        return content;
    }

    /// <summary>
    /// Overloaded method used from command line.
    /// </summary>
    /// <param name="matricula"></param>
    /// <param name="reCaptchaCode"></param>
    /// <returns></returns>
    public string sendData(plate matricula, string reCaptchaCode)
    {
        return sendData(matricula.plate_str, matricula.date, reCaptchaCode, matricula.id_plate);
    }

    /// <summary>
    /// Mimics sending form to site, with the proper information
    /// to sendData, previously we called extractContentFromPage
    /// 20170801: Version 2: extended for new IMM page using recaptcha
    /// Date encoding reference: see http://stackoverflow.com/questions/575440/url-encoding-using-c-sharp/7427556#7427556
    /// </summary>
    public string sendData(string matricula,string fecha,string reCaptchaCode,int id_plate)
    {

        string responseString = "";
        //Step 1: Encode data to be sent, base format:
        //es_montevideo=2&matricula=sbu6038&padron=&fecha_multa%5Bdate%5D=02%2F03%2F2017&captcha_sid=1384758&captcha_token=5a3dd71b9a4c12fb1e19750acccb340c&captcha_response=Google+no+captcha&g-recaptcha-response=03AJz9lvRU_wihe3xko-XcdjTAaB15_jsfknDI2eQMQzohUi_PolqvJUaY-d_uQMaTtxMmYM1UflT0CDYycRfvfoO9R-Sx32mvU0fP7GuAjLUpl9E5jsu_07gpEwOY-8SW6iSo1Bjd3yGscQN9NFROITnwxwf5kNJIC2196RRDlD3F2mmtsItqq5byC_rL3U832eAAEsl4UmarIkjwkCT3kKf1RvPc_f9iOmwP21EQkzYJN6ls3WtE29QfrAOpnUeIVBXTqmX8p7E6wEB4EUDKScV3_bL-S-1E66W2PaXv4Rc8u9fn9jrSt69bvDOQj5BKDcjs49l0wWB7oWZ5O1V3yW0tIJGp5qd_1g&op=Consultar&form_build_id=form-OSLq4CV94kEGMT-Cw-N-Opn_Jn5-c0rO3v49SsAYYps&form_id=consulta_multas_form
        //es_montevideo=2&matricula={0}&padron=&fecha_multa%5Bdate%5D={1}&captcha_sid={2}&captcha_token={3}&captcha_response=Google+no+captcha&g-recaptcha-response={4}&op=Consultar&form_build_id={5}&form_id=consulta_multas_form

        string pD = "es_montevideo=2&matricula={0}&padron=&fecha_multa%5Bdate%5D={1}&captcha_sid={2}&captcha_token={3}&captcha_response=Google+no+captcha&g-recaptcha-response={4}&op=Consultar&form_build_id={5}&form_id=consulta_multas_form";

        //IMPORTANT: meaning of different places
        //few notes: 0 - plate: replace ' ' with '+'
        //           1 - date in the format dd/mm/yyyy uri encoded. NO TIME, NO SECONDS!!!!
        //           2 - captcha_sid
        //           3 - captcha_token
        //           4 - g-recaptcha_response = recaptcha_value expected (provided via API)
        //           5 - form_build_id

        string mat = matricula.Replace(' ', '+'); //Replace space by '+' in plates
        string fecha_cut = WebUtility.UrlEncode(fecha.Substring(0, 10));
        string msg = string.Format(pD, mat, fecha_cut, captcha_sid, captcha_token, reCaptchaCode, form_build_id);
        //Step 2: send the data.
        OnMsg(string.Format("Enviando matricula:{0},fecha={1},reCaptcha(solo los primeros 10 char)={2}", matricula, fecha, reCaptchaCode.Substring(0,10)));
        try
        {
            responseString = Utilities.postCall(msg, url_infracciones);//Send the encoded data, wait for the answer
        }
        catch (System.Net.WebException wex)
        {
            OnMsg("Excepción en PostCall al invocar la página de infracciones. Lo procesamos como un fallo para que se repita todo");
            OnMsg(string.Format("Detalles del error: {0}" , wex.Message));
            return null;
        }
        catch (Exception ex)
        {
            OnMsg("Error general en PostCall al invocar la página de infracciones. Repetimos el procedimiento");
            OnMsg(string.Format("Detalles del error: {0}", ex.Message));
            return null;
        }
        
        string response_filename = "response_" + mat + "_" + id_plate + ".htm";         
        //Step 3(optional): save page returned to disk for later analysis
        Utilities.writeAllFile(Path.Combine(Utilities.BASE_DIR_RESPONSES,response_filename), responseString); //Save response for analysis, just in case
        OnMsg(string.Format("Salvando respuesta htm en: {0}", response_filename));
        last_saved_file = response_filename;
        return responseString;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="id_plate"></param>
    private void markPlateAsProcessed(int id_plate, Utilities.processResult pResult, string reason)
    {
        plateCtl pc = new plateCtl(currConn);
        pc.updatePlate(id_plate, pResult, reason);
    }


    /// <summary>
    /// Scan for errors, return true if error,otherwise false
    /// STORE error message on statusMsg variable
    /// 
    /// 20170804: Currently we have a issue: it can happen that we have an error page and also correct result:
    /// Undefined index: HTTP_USER_AGENT en <em class="placeholder">consulta_multas_form_submit()</em> (línea <em class="placeholder">201</em> de <em class="placeholder">/datos/www/montevideo.gub.uy/html/sites/default/modules/custom/consulta_multas/consulta_multas.module</em>)
    /// and above there is a message:
    ///  <div id='replace_textfield_div'>No se generaron archivos fotográficos para el día 26/03/2017  de la matrícula SBN 006.</div>
    /// This means we could successfully access to the page and we need to move on.
    /// </summary>
    /// <param name="response"></param>
    /// <param name="startString"></param>
    /// <param name="endString"></param>
    /// <returns></returns>
    public bool checkError(string response, string startString, string endString)
    {
        string fragment;
        string msgError = Utilities.between(response, startString, endString, out fragment);
        if (fragment == "") return false; //if fragment empty then MAYBE is not an erro
        if (msgError.Length < 2) return false;
        statusMsg = msgError.Substring(2); //skip two first characters (">)
        statusMsg = Regex.Replace(statusMsg, "<.*?>", string.Empty); //Strip all HTML tags
        return true;
    }

    /// <summary>
    /// 2010803:Shortened version to call saveImages
    /// Separation between saving image to disk and saving to DB
    /// adding updateDB option, for testing purposes without touching database
    /// </summary>
    /// <param name="response"></param>
    /// <param name="mat"></param>
    /// <param name="counter"></param>
    public void saveImagesJpg(string response, plate mat, int counter, string fullPath, bool updateDB)
        {
            string img_name=saveImagesJpg(response,mat.plate_norm,mat.id_plate,mat.date,counter, fullPath);
            if (!updateDB)
            {
                OnMsg("WARNING: save a DB deshabilitado!!!");
            }
            else
            {
                saveImgDB(img_name, mat.id_plate);
                //Raise an event to display the image 
                OnMsg(string.Format("Guardando imagen {0} en {1}", counter, img_name));
                OnPicture(new imgData() { imageNumber = counter, picturePath = Path.Combine(fullPath, img_name) });
            }
    }

    /// <summary>
    /// extracts an image from a html tag with picture encoded base 64
    /// This version was simplified. There is one picture for response provided.
    /// </summary>
    /// <param name="response">html code to scan</param>
    /// <param name="mat">mat related</param>
    /// <param name="id_plate">id plate related</param>
    /// <param name="fecha">date related</param>
    private string saveImagesJpg(string response, string mat, int id_plate, string fecha,int counter, string fullPath)
    {
        string detect_image = @"<img\ssrc='data://image/png;base64,(?<base64>.*?)'";
        Match match = Regex.Match(response, detect_image);
        string img_name;
        string img_base_64;
        if (match.Success) 
        {
            img_base_64 = match.Groups["base64"].Value;
            img_name = @"img_" + mat.Replace(' ', '_') + "_"
                                        + Utilities.reversedDate(fecha) + "_"
                                        + counter + ".jpg";
            //Few test show that best fit is jpeg at 50% ina a compromise size/quality
            saveJpg(img_base_64, img_name, 50, Path.Combine(fullPath, img_name));
            return img_name;
        }
        OnMsg(string.Format("WARNING: No image found on matricula: {0}, id_plate", mat,id_plate));
        return "";
    }
    
    /// <summary>
    /// Saves data to database
    /// WARNING: currently assuming that every images will be different.
    /// I mean I'M NOT CONSIDERING CASE WHEN I HAVE REPEATEAD plates for examples. 
    /// This need to be analyzed.
    /// </summary>
    /// <param name="img_name"></param>
    /// <param name="id_plate"></param>
    private void saveImgDB(string img_name,int id_plate)
    {
        photo p = new photo() { filename = img_name, id_plate = id_plate };
        photoCtl pc = new photoCtl(currConn);
        pc.addPhoto(p);
    }

        /// <summary>
        /// Save base64 image as jpeg file
        /// Discussion: https://msdn.microsoft.com/en-us/library/bb882583(v=vs.110).aspx
        /// </summary>
        /// <param name="base64String"></param>
        /// <param name="path"></param>
        /// <param name="Quality"></param>
        /// <param name="fullPath"></param>
        private void saveJpg(string base64String, string path, byte Quality, string fullPath)
    {

        byte[] bytes = Convert.FromBase64String(base64String);
        Image image;
        MemoryStream ms = new MemoryStream(bytes);
        image = Image.FromStream(ms);
        Encoder myEncoder = Encoder.Quality;
        EncoderParameters myEncoderParameters = new EncoderParameters(1);
        EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, Quality);
        myEncoderParameters.Param[0] = myEncoderParameter;
        image.Save(fullPath, System.Drawing.Imaging.ImageFormat.Jpeg);
        ms.Close();
    }
    
    /// <summary>
    /// extract the value after some attribute
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    private string extract_value(string name,string content)
        {
            return extract_value(name, content, "value");
        }

    /// <summary>
    /// Extract a value from a content, scanning a tag inside it.
    /// </summary>
    /// <param name="name"></param>
    /// <param name="content"></param>
    /// <param name="tag"></param>
    /// <returns></returns>
    private string extract_value(string name, string content, string tag)
        {
            string temp;
            try
            {
                temp = content.Substring(content.IndexOf(name));
                if (temp == "") return string.Empty;
                temp = temp.Substring(temp.IndexOf( tag + "=" ) + tag.Length + 2); //2 refers the " and 1 more character to properly position.
                if (temp == "") return string.Empty;
                temp = temp.Substring(0, temp.IndexOf('"'));
                if (temp == "") return string.Empty;
                temp = temp.Replace("\"", "");
                return temp;
            }
            catch (Exception ex)
            {

                Console.WriteLine("ooops... Error when extracting value {0} from content! Error: {1}", name, ex.Message);
                return string.Empty;
            }
        }

}

//save picture in a captcha cache (I found the cache use a reference number. There is a chance in a million to be repeated,
//References:
//http://stackoverflow.com/questions/11511511/how-to-save-a-png-image-server-side-from-a-base64-data-string
//http://stackoverflow.com/questions/18827081/c-sharp-base64-string-to-jpeg-image
//http://stackoverflow.com/questions/5400173/converting-a-base-64-string-to-an-image-and-saving-it

//but still is a chance. Is it useful to reuse it? Don'know yet, but in 6 iterations we already have around 2000 querys to be made,
//and also with a high chance of failure, so it could be about 4000 tries, whici is about 1/250 to repeat the captcha image.

/*
/// <summary>
/// Save base64 image as file
/// Discussion: http://stackoverflow.com/questions/5400173/converting-a-base-64-string-to-an-image-and-saving-it
/// HOWEVER: make image.Save INSIDE , otherwise there will be an error.
/// </summary>
/// <param name="base64String"></param>
/// <param name="path"></param>
private void savePng(string base64String, string path)
{
byte[] bytes = Convert.FromBase64String(base64String);
string fullPath = Path.Combine(Utilities.BASE_DIR_IMAGES, path);
Image image;
MemoryStream ms = new MemoryStream(bytes);
image = Image.FromStream(ms);
image.Save(fullPath, System.Drawing.Imaging.ImageFormat.Png);
ms.Close();
}

    /// 20172907: La página de la Intendencia cambió hace meses y me di por vencido
/// Sin embargo pasaron 2 cosas trascendentes:
/// 1-NO cerré la aplicación, la cual siguió creciendo
/// 2-Volví a la carga, para encontrar que aunque está mas difícil, SI es posible obtener las imagenes. Esto es gracias a un BUG que tiene la aplicación
/// Si la aplicación continua con este 'problema', podré seguir extrayendo imagenes, en caso contrario se volverá imposible.
/// 3-Alguien dijo fácil? Es difícil, pero no es imposible. Asi que volvemos a la carga.
/// TODO: A way of improving recaptcha solution time
/// TODO: Make a counter to evaluate recaptchas solved and wrong.

    /// <summary>
/// Process response, depending on the results we do one thing or another.
/// 20170802: OBSOLETE, for consulting, just delete it after!
/// </summary>
/// <param name="responseString"></param>
/// <param name="matricula"></param>
/// <param name="id_plate"></param>
/// <param name="captcha_answer"></param>
    public bool processResponse(string responseString,string matricula,int id_plate,string captcha_answer,string fecha)
    {
        statusMsg = "";//reset status msg
        string mat = matricula.Replace(' ', '_');
      
        if (encodedImageExists(responseString))
        {
            OnMsg("EXITO!: procesando imagenes...");
            //Case 1:If we got here, we suceded

            //saveImagesJpg(responseString,mat,id_plate,fecha); //-Save the image(s) related (inside saveImages) and also update database
            
            //Mark the plate as correctly processed.
            OnMsg(string.Format("marcando matricula ID {0} como exitosa" ,id_plate));
            markPlateAsProcessed(id_plate, "Processed OK!");
            OnMsg(string.Format("Actualizando captcha obtenido con la respuesta {0} como correcto",captcha_answer));
          //  updateCaptcha(captcha_answer, true); //save data always, whenever it has succeed or not, flagging properly.
            return true;
        }
        OnMsg("No tuvimos éxito, determinando la razón...");
        //If not Image detected, two chances: Captcha error or simply there is no images (some kind of error here)
        bool CaptchaSuccess = false;
        //We didn't have images.
        //Two chances: captcha error or some problem related (there are plates without images also)
        //Example: 4678 JGV has not image records
        //In this case we need to move forward, the process was valid, but not images were found.
        //Also, a reason need to be stored with the images to explain what happened. 
        if (!checkError(responseString))
        {
            //We didn't have an error. Could it be possible there is another reason
            string reason;
            if (checkReason(responseString,out reason)) //Check if there is a reason, return in the reason parameter
            {
                OnMsg(string.Format("La página retorna la siguiente razón:{0}",reason));
                OnMsg(string.Format("Asi que despues de todo, el proceso salió bien! Se marca la matricula ID {0} como procesada OK", id_plate));
                //Yes there was a problem getting the images, that wasn't our fault, is page fault!
                CaptchaSuccess = true; //so the captcha was ok then!
                //we also have to mark the image as processed.
                markPlateAsProcessed(id_plate,reason);
            }
        } else
        {
            OnMsg(string.Format("No hubo suerte, hay que probar de nuevo, la razón es: {0}", statusMsg));
        }

        //updateCaptcha(captcha_answer, CaptchaSuccess); //save data always, whenever it has succeed or not, flagging properly.
        return CaptchaSuccess;
        //What else is needed? Continue polishing error system. Make some more tests.
    }

 /// <summary>
    /// Extract error if possible
    /// What we know: error is sourrounded with <div class="messages--error messages error">....</div>
    /// So we'll try to isolate the problem
    /// </summary>
    /// <param name="response"></param>
    private bool checkReason(string response, out string why)
    {
        why = "";
        string fragment;
        string reason = Utilities.between(response, "replace_textfield_div", "</div>", out fragment).Trim();
        reason = reason.Replace("\n", "");
        reason = reason.Replace("\t", "");
        reason = reason.Replace(">", "");
        reason = reason.Replace("'", "");
        reason = reason.Replace("\"", "");
        if (reason.Length!=0)
        {
            statusMsg = reason;
            why = reason;
            return true; ;
        }
        if (response!="")
        {
            statusMsg = string.Format("Hubo un problema indeterminado para obtener los datos, verificar los datos {0}", response);
            return false;
        }
        statusMsg = string.Format("Hubo un problema indeterminado para obtener los datos y tampoco pude grabar el archivo. Esto es un desastre...");
        return false;
    }

     /// <summary>
    /// Checks if the page has an encoded image
    /// In fact this is a little redundant and it should be improved.
    /// </summary>
    /// <param name="response"></param>
    /// <returns></returns>
    private bool encodedImageExists(string response)
    {
        string detect_image = @"<img\ssrc='data://image/png;base64,(.*?)'";
        Match match = Regex.Match(response, detect_image);
        return match.Success;
    }
     */
