﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class pageCtl
    {
        List<page> pages = new List<page>();
    DbConnection currentConn;
    string insertPage = "insert into pages(url,parsed) values ('{0}',{1})";
    string findPage_by_id = "Select * from pages where id_page={0}";
    string findPage_by_url = "Select * from pages where url='{0}'";
    string getLastPage_sql = "Select url from pages order by id_page desc";
   // string selectPages = "Select * from pages";

    public pageCtl(DbConnection sqlConn)
        {
        currentConn = sqlConn;
        }

    public void addPage(page p)
    {
        try
        {
            using (DbCommand cmd = currentConn.CreateCommand())
            {
                int flag = (p.parsed) ? 1 : 0;
                cmd.CommandText = string.Format(insertPage,p.url,flag);
                cmd.ExecuteNonQuery();
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    public int findPage(int id_page)
    {
        try
        {
            using (DbCommand cmd = currentConn.CreateCommand())
            {
                cmd.CommandText = string.Format(findPage_by_id,id_page);
                return (int)cmd.ExecuteScalar(); //WARNING: possible return incorrectly results
            }

        }
        catch (Exception)
        {

            throw;
        }
    }

    public int findPage(string url)
    {
        try
        {
            using (DbCommand cmd = currentConn.CreateCommand())
            {
                cmd.CommandText = string.Format(findPage_by_url, url);
                object obj= cmd.ExecuteScalar();
                int result = -1;
                if (obj!=null)
                {
                    result= int.Parse(obj.ToString());
                }
                return result ; //WARNING: possible return incorrectly results
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            throw;
        }
    }

    /// <summary>
    /// get the url of last page scanned.
    /// </summary>
    /// <returns></returns>
    public string getLastPage()
    {
        try
        {
            using (DbCommand cmd = currentConn.CreateCommand())
            {
                cmd.CommandText = getLastPage_sql;
                object obj = cmd.ExecuteScalar();
                string result = "";
                if (obj != null)
                {
                    result = obj.ToString();
                }
                return result.ToString(); //WARNING: possible return incorrectly results
            }
        }
        catch (Exception)
        {
            throw;
        }
    }


}


