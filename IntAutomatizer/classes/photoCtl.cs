﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class photoCtl
    {

    DbConnection currentConn;
    string insertPhoto = "insert into photos(id_plate,filename) values ({0},'{1}')";
    public List<plate> plates = new List<plate>();

    public photoCtl(DbConnection sqlConn)
    {
        currentConn = sqlConn;
    }

    /// <summary>
    /// Add plate to database
    /// </summary>
    /// <param name="p"></param>
    public void addPhoto(photo p)
    {
        try
        {
            using (DbCommand cmd = currentConn.CreateCommand())
            {
                cmd.CommandText = string.Format(insertPhoto, p.id_plate, p.filename);
                cmd.ExecuteNonQuery();
            }
        }
        catch (Exception)
        {
            throw;
        }
    }
}