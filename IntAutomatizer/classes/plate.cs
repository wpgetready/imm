﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// 20170310: added plate_norm as plate normalization, used to make sure search works alright.
/// 20170806: processed changed from bool to int to be used in more scenarios.
/// </summary>
    public class plate
    {
        //id_plate,id_page,plate,date,place,intervention,article,ur
        public int id_plate;
        public int id_page;
        public string plate_str;
        public string plate_norm;
        public string date;
        public string place;
        public string intervention;
        public string article;
        public int ur;
        public int processed;
        public DateTime date_norm;
    }

