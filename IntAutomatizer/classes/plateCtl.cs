﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

class plateCtl
    {
        DbConnection currentConn;
        string insertPlate = "insert into plates(id_page,plate,date,place,intervention,article,ur,processed,plate_norm,date_norm) values ({0},'{1}','{2}','{3}','{4}','{5}',{6},{7},'{8}','{9}')";
        string findPlate_by_id = "Select * from plates where id_plate={0}";
        string findPlate_by_plate_norm = "Select * from plates where plate_norm='{0}'";
        string selectPlateProcessed = "Select * from plates where processed={0}";
        string updatePlateSQL = "Update plates set processed={0},reason='{1}' where id_plate={2}";

    public List<plate> plates = new List<plate>();

    public plateCtl(DbConnection sqlConn)
        {
            currentConn = sqlConn;
        }

        /// <summary>
        /// Add plate to database
        /// </summary>
        /// <param name="p"></param>
        public void addPlate(plate p )
        {
        try
        {
            using (DbCommand cmd = currentConn.CreateCommand())
            {
                //int flag = (p.processed) ? 1 : 0;
                cmd.CommandText = string.Format(insertPlate, p.id_page, p.plate_str, p.date, p.place, p.intervention, p.article, p.ur, p.processed,p.plate_norm,p.date_norm);
                cmd.ExecuteNonQuery();
            }
        }
        catch (Exception)
        {
            throw;
        }
        }

    /// <summary>
    /// Find plate using id_plate. 
    /// Return true if it finds it, false otherwise.
    /// Version 1:
    /// Some details:
    /// -Not checked if id_plate is repeated
    /// -Maybe it would be wise returning plate info.
    /// </summary>
    /// <param name="id_plate"></param>
    /// <returns></returns>
    public bool findPlate(int id_plate)
    {
        try
        {
            using (DbCommand cmd = currentConn.CreateCommand())
            {
                cmd.CommandText = string.Format(findPlate_by_id, id_plate);
                int result = cmd.ExecuteNonQuery(); //WARNING: possible return incorrectly results
                return (result != 0);
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    public bool findPlate (string plate)
    {
        try
        {
            using (DbCommand cmd = currentConn.CreateCommand())
            {
                cmd.CommandText = string.Format(findPlate_by_plate_norm, plate);
                int result = cmd.ExecuteNonQuery(); //WARNING: possible return incorrectly results
                return (result != 0);
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Mark a plate as processed, pass a reason optionally.
    /// </summary>
    /// <param name="id_plate"></param>
    /// <param name="reason"></param>
    public void updatePlate(int id_plate,Utilities.processResult processedLevel, string reason)
    {
        try
        {
            using (DbCommand cmd = currentConn.CreateCommand())
            {
                //1 means processed (true)
                cmd.CommandText = string.Format(updatePlateSQL, processedLevel,reason,id_plate );
                cmd.ExecuteNonQuery();
            }
        }
        catch (Exception)
        {
            throw;
        }
    }
    /// <summary>
    /// Load plates to the list    
    /// 20170310: added plate normalization to the table
    /// </summary>
    public void loadPlates(bool processed)
    {
        
        //Reset list
        plates.Clear();
        plate p;
        try
        {
            using (DbCommand cmd = currentConn.CreateCommand())
            {
                int flag = (processed) ? 1 : 0;
                cmd.CommandText = string.Format(selectPlateProcessed, flag);
                using (System.Data.Common.DbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        p = new plate();
                        p.id_plate = int.Parse(reader["id_plate"].ToString());
                        p.id_page = int.Parse(reader["id_page"].ToString());
                        p.plate_str =(string)reader["plate"];
                        p.date = (string)reader["date"];
                        p.intervention = (string)reader["intervention"];
                        p.article = (string)reader["article"];
                        p.ur = int.Parse(reader["ur"].ToString());
                        p.processed = int.Parse(reader["processed"].ToString());
                        p.plate_norm = (string)reader["plate_norm"]; //normalizes plate to easier searches.
                        //p.date_norm = reader.GetDateTime(11);
                        p.date_norm =(DateTime)reader["date_norm"];
                        plates.Add(p);
                    }
                }
                cmd.Dispose();
            }
        }
        catch (Exception ex)
        {
            MessageBox.Show(ex.Message);
            throw;
        }
    }


    /// <summary>
    /// Save all plate data. WARNING, data could be repeated if no properly processed.
    /// </summary>
    public void savePlates()
    {
        try
        {
            using (DbCommand cmd = currentConn.CreateCommand())
            {
                using (DbTransaction tr = currentConn.BeginTransaction())
                {
                    foreach (plate p in plates)
                    {
                        addPlate(p);
                    }
                    tr.Commit(); //the rollback is not explicit, but it will happen in case of error.
                }
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    }



