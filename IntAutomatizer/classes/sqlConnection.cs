﻿using System.Data.Common;
using System.Data.SQLite;
using System.IO;


class sqlConnection
    {
        public static DbConnection createConnection()
        {
        string dbFile = Path.Combine(Utilities.BASE_DIR_DATABASE, "multas.sqlite");
        string connString = string.Format(@"Data Source={0}; Pooling=false; FailIfMissing=false;datetimeformat=CurrentCulture", dbFile);
        SQLiteFactory factory = new SQLiteFactory();
        DbConnection conn = factory.CreateConnection();
        conn.ConnectionString = connString;
        conn.Open();
        return conn;
        }
    }

