﻿using IntAutomatizer.forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IntAutomatizer
{
    public partial class Form1 : Form
    {
 
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lstInfo.Items.Clear();
            //Connect events with the form
            callImpo ci = new callImpo();
            ci.Msg += new MessageEventHandler(receiveMsg);
            ci.start();
            MessageBox.Show("Terminada");
        }

        private void receiveMsg(object sender, string msg)
        {
            lstInfo.Items.Add(msg);
            Utilities.Log(msg);
            lstInfo.Refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form frm = new frmCaptcha();
            frm.Show();
         }

        private void lstInfo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }


}

