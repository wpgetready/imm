﻿namespace IntAutomatizer.forms
{
    partial class frmCaptcha
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtMatricula = new System.Windows.Forms.TextBox();
            this.txtFecha = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.imgCaptcha = new System.Windows.Forms.PictureBox();
            this.txtCaptcha = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.picMulta1 = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.picMulta2 = new System.Windows.Forms.PictureBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnSimulation = new System.Windows.Forms.Button();
            this.txtResponseSimulator = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnSendToWeb = new System.Windows.Forms.Button();
            this.lblInfo = new System.Windows.Forms.Label();
            this.lblCounter = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.imgCaptcha)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMulta1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMulta2)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtMatricula
            // 
            this.txtMatricula.Location = new System.Drawing.Point(84, 12);
            this.txtMatricula.Name = "txtMatricula";
            this.txtMatricula.Size = new System.Drawing.Size(155, 20);
            this.txtMatricula.TabIndex = 0;
            // 
            // txtFecha
            // 
            this.txtFecha.Location = new System.Drawing.Point(84, 51);
            this.txtFecha.Name = "txtFecha";
            this.txtFecha.Size = new System.Drawing.Size(155, 20);
            this.txtFecha.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Matricula";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Fecha";
            // 
            // imgCaptcha
            // 
            this.imgCaptcha.Location = new System.Drawing.Point(288, 12);
            this.imgCaptcha.Name = "imgCaptcha";
            this.imgCaptcha.Size = new System.Drawing.Size(263, 83);
            this.imgCaptcha.TabIndex = 2;
            this.imgCaptcha.TabStop = false;
            // 
            // txtCaptcha
            // 
            this.txtCaptcha.Location = new System.Drawing.Point(84, 88);
            this.txtCaptcha.Name = "txtCaptcha";
            this.txtCaptcha.Size = new System.Drawing.Size(155, 20);
            this.txtCaptcha.TabIndex = 0;
            this.txtCaptcha.TextChanged += new System.EventHandler(this.txtCaptcha_TextChanged);
            this.txtCaptcha.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCaptcha_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Captcha";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(13, 257);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(549, 449);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.picMulta1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(541, 423);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // picMulta1
            // 
            this.picMulta1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picMulta1.Location = new System.Drawing.Point(3, 3);
            this.picMulta1.Name = "picMulta1";
            this.picMulta1.Size = new System.Drawing.Size(535, 417);
            this.picMulta1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picMulta1.TabIndex = 0;
            this.picMulta1.TabStop = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.picMulta2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(541, 423);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // picMulta2
            // 
            this.picMulta2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picMulta2.Location = new System.Drawing.Point(3, 3);
            this.picMulta2.Name = "picMulta2";
            this.picMulta2.Size = new System.Drawing.Size(535, 417);
            this.picMulta2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picMulta2.TabIndex = 1;
            this.picMulta2.TabStop = false;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnSimulation);
            this.tabPage3.Controls.Add(this.txtResponseSimulator);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(541, 423);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "testDiversos";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnSimulation
            // 
            this.btnSimulation.Location = new System.Drawing.Point(384, 12);
            this.btnSimulation.Name = "btnSimulation";
            this.btnSimulation.Size = new System.Drawing.Size(136, 26);
            this.btnSimulation.TabIndex = 2;
            this.btnSimulation.Text = "Simular respuesta";
            this.btnSimulation.UseVisualStyleBackColor = true;
            this.btnSimulation.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtResponseSimulator
            // 
            this.txtResponseSimulator.Location = new System.Drawing.Point(124, 16);
            this.txtResponseSimulator.Name = "txtResponseSimulator";
            this.txtResponseSimulator.Size = new System.Drawing.Size(254, 20);
            this.txtResponseSimulator.TabIndex = 1;
            this.txtResponseSimulator.Text = "respuesta_513291_BP6Zh_con_imagenes.txt";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Archivo de prueba";
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(117, 114);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(122, 23);
            this.btnNext.TabIndex = 4;
            this.btnNext.Text = "Siguiente Matricula";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnSendToWeb
            // 
            this.btnSendToWeb.Location = new System.Drawing.Point(430, 114);
            this.btnSendToWeb.Name = "btnSendToWeb";
            this.btnSendToWeb.Size = new System.Drawing.Size(117, 23);
            this.btnSendToWeb.TabIndex = 4;
            this.btnSendToWeb.Text = "Enviar al sitio";
            this.btnSendToWeb.UseVisualStyleBackColor = true;
            this.btnSendToWeb.Click += new System.EventHandler(this.btnSendToWeb_Click);
            // 
            // lblInfo
            // 
            this.lblInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblInfo.Location = new System.Drawing.Point(20, 149);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(538, 94);
            this.lblInfo.TabIndex = 5;
            this.lblInfo.Text = "Información goes here";
            this.lblInfo.Click += new System.EventHandler(this.label4_Click);
            // 
            // lblCounter
            // 
            this.lblCounter.AutoSize = true;
            this.lblCounter.Location = new System.Drawing.Point(288, 114);
            this.lblCounter.Name = "lblCounter";
            this.lblCounter.Size = new System.Drawing.Size(13, 13);
            this.lblCounter.TabIndex = 6;
            this.lblCounter.Text = "--";
            // 
            // frmCaptcha
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(581, 727);
            this.Controls.Add(this.lblCounter);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.btnSendToWeb);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.imgCaptcha);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCaptcha);
            this.Controls.Add(this.txtFecha);
            this.Controls.Add(this.txtMatricula);
            this.Name = "frmCaptcha";
            this.Text = "frmCaptcha";
            this.Load += new System.EventHandler(this.frmCaptcha_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imgCaptcha)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picMulta1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picMulta2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtMatricula;
        private System.Windows.Forms.TextBox txtFecha;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox imgCaptcha;
        private System.Windows.Forms.TextBox txtCaptcha;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.PictureBox picMulta1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.PictureBox picMulta2;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnSendToWeb;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnSimulation;
        private System.Windows.Forms.TextBox txtResponseSimulator;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblCounter;
    }
}