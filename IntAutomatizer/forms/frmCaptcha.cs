﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Common;

namespace IntAutomatizer.forms
{
    public partial class frmCaptcha : Form
    {

        private int counter;
        plate current_plate;
        static DbConnection dbc = sqlConnection.createConnection();
        plateCtl matriculas = new plateCtl(dbc);
        consultaInfracciones cf = new consultaInfracciones();
        bool captchaSuccess = false; //Inicialmente tuvimos éxito para empezar

        public frmCaptcha()
        {
            InitializeComponent();
        }

        private void frmCaptcha_Load(object sender, EventArgs e)
        {
            mainLogic();
        }

        //logica principal del programa
        public void mainLogic()
        {
            //Load plates only those not processed
            matriculas.loadPlates(false);
        
                
        }

        /// <summary>
        /// TODO: Improve method since the new class uses events.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNext_Click(object sender, EventArgs e)
        {
            if (matriculas.plates.Count > 0)
            {
                if (counter>=matriculas.plates.Count)
                {
                    MessageBox.Show("Fin de imagenes a procesar!");
                    return;
                }
                lblCounter.Text = counter + " de " + matriculas.plates.Count;
                picMulta1.ImageLocation = "";
                picMulta2.ImageLocation = "";
             //   cf.img1 = picMulta1;
             //   cf.img2 = picMulta2;
                current_plate = matriculas.plates[counter];
                //Display values in the appropiate textbox
                txtFecha.Text = current_plate.date;
                txtMatricula.Text = current_plate.plate_str;
                //Display the captcha:
                txtCaptcha.Text = "WAIT.....";
                txtCaptcha.Refresh();
           //     imgCaptcha.ImageLocation = cf.callPage(current_plate.id_plate); //invoke the page, retrieve data, save captcha image.
                txtCaptcha.Text = "";
                lblInfo.Text = "Procesando " + txtMatricula.Text + "...";
                txtCaptcha.Focus();
                btnNext.Enabled = false;
            }
            else
            {
                MessageBox.Show("El sistema no pudo traer matriculas sin procesar! Están todas procesadas?");
            }
        }

        private void btnSendToWeb_Click(object sender, EventArgs e)
        {
            //Ok seguimos adelante.
            //Si no tenemos el captcha, no se sigue
            if (txtCaptcha.Text=="")
            {
                MessageBox.Show("Falta el captcha!");
                return;
            }
            captchaSuccess = cf.sendData(txtMatricula.Text, txtFecha.Text, txtCaptcha.Text, current_plate.id_plate);
            //check info and display it
            if (captchaSuccess)
            {
                lblInfo.Text = cf.statusMsg + ". PROCESO TERMINADO , matrícula " + txtMatricula.Text;
                counter++;
            }
            else
            {
                lblInfo.Text = cf.statusMsg + "\n. Error al procesar la matrícula, Se repite proceso para " + txtMatricula.Text + "\n Pulsar siguiente matricula para repetir.";
                    
            }
            btnNext.Enabled = true;
            
            

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string fullPath = Path.Combine(Utilities.BASE_DIR_RESPONSES, txtResponseSimulator.Text);
            if (!File.Exists(fullPath))
            {
                MessageBox.Show("No existe el archivo!");
                return;
            }
            //Read the file. THIS IS A TEST!!!!
             cf.processResponse(Utilities.readFile(fullPath), txtMatricula.Text, -1 ,"no_answer" , "11/01/2017 11:50");
            lblInfo.Text = cf.statusMsg;
        }

        private void txtCaptcha_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCaptcha_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.KeyValue == 13)
            {
                btnSendToWeb_Click(null, null);
            }
        }
    }
}
