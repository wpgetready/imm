﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// This little program attempts to make all those common queries flying around my head about different data I wish always to know.
/// </summary>
namespace MostCommonQueries
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    static class qry
    {
        static string MORE_THAN_ONCE = "select count (*) from (select count(*) from plates group by plate having count(*)>2)"; //retorna cantidad de vehiculos multados mas de una vez.
        static string TOP_TEN = "   "; //10 vehiculos mas multados, sin discriminar motos.
        static string DUAL_TICKET = "select count(*) from (Select plate from plates where instr(article,'103')>0 intersect Select plate from plates where instr(article,'106')>0)"; //multados por exceso y cruzar roja.
        static string HOW_MANY_SO_FAR = "select count (distinct plate) from plates"; //cantidad de matrículas no repetidas.
        static string NOTIF_QTY = "select count(*) from plates"; //cantidad de notificaciones
        static string TOTAL_UR = "Select sum (ur) from plates"; //Costo unidad reajustable = 946, en dolares es x*946/29 = actualmente unos 2 millones de dólares.
    }
}
