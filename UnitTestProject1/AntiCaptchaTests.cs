﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using IntAutomatizer.classes.Anticaptcha;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;


[TestClass()]
    public class AntiCaptchaTests
    {
        [TestMethod()]
        public void callAPITest()
        {
        AntiCaptcha ac = new AntiCaptcha();
       // Assert.Fail("Anduvo mal");
        // ACGetTaskResult result=   ac.invokeAntiCaptcha();
        //Suspendido porque tiene costo
        }

    /// <summary>
    /// Esta es una prueba completa para determinar si pasamos a la siguiente página que es nuestro objetivo final
    /// Para ello es preciso realizar un bucle completo del proceso con un valor tentativo.
    /// Lo interesante y complicado de este proceso es atomico, no puedo desarmarlo para hacer pruebas por separado
    /// por un problema de datos y timing, lo que complica el test.
    /// </summary>
    /// 
    /// 
    [TestMethod()]
    public void completeAPITest()
        {
            plate matricula = new plate();
        consultaInfraccionesV2 CI = new consultaInfraccionesV2();
        CI.Msg += CI_Msg;

        //Simulo una matricula para usar en la prueba
            matricula.plate_str = "sbu6038";
            matricula.plate_norm = "sbu6038";
        matricula.date = "02/03/2017";
            matricula.id_plate = 99999; //de momento no se usará para este test,pero es necesario para la invocación de sendData
            

            AntiCaptcha ac = new AntiCaptcha();
            ACGetTaskResult result=   ac.invokeAntiCaptcha();
            if (result == null)
            {
            Assert.Fail("Fallo: no se pudo obtener el reCaptcha code");
            }
            //Si tengo el recaptcha, intento obtener la página de consulta.
            string page_content = CI.extractContentFromPage(consultaInfraccionesV2.url_infracciones, matricula);
            if (page_content == null)
            {
                Assert.Fail(("Fallo: no pude obtener la página de la Intendencia"));
            }
            string response = CI.sendData(matricula, result.solution.gRecaptchaResponse);
        //Tenemos la página
        if (response == null) //Hubo algun error al traer la página: el servidor caído, Internet caída o algo que anduvo mal.
            {
                Assert.Fail(("Fallo: la página no pudo obtenerse"));
            }
            //Estamos frente a dos casos: o tuvimos éxito y fallamos.
            //como detectamos esto? Si tenemos un tag del tipo <div id='datos_multa'> eso quiere decir que tuvimos éxito.
            if (response.Contains("id='datos_multa'"))
            {
                //1-Tuvimos éxito y llegamos a la página, pero es preciso invocar Ajax para obtener las imagenes
                //Tenemos la página que estamos buscando, es hora de invocar usando Ajax
                CI.ajaxCall(response,matricula);
            }
            else
            {
                if (CI.checkError(response, "messages--error messages error", "</div>")) //true si hubo un error, almacena el error en la variable statusMsg
                {
                    Assert.Fail(string.Format("Hubo un error al obtener la página:{0}", CI.statusMsg));
                }
                else
                {
                    //Hay un error de otro tipo: la página es incorrecta, o no hay fotos o que?
                    Assert.Fail(string.Format("Error indeterminado con la matricula {0}", matricula.plate_norm));
                }
            }
    }
    
    /// <summary>
    /// Prueba de invocacion de Ajax, debido a que es necesario  testear si el header que estoy creando es válido 
    /// 
    /// </summary>
    [TestMethod()]
        public void testPostCallAjax()
        {
        Utilities.postCallAjax("<h1>test content</h1>", consultaInfraccionesV2.url_infracciones);
        }

    /// <summary>
    /// Esta es una prueba para obtener las imagenes a partir de los datos JSOn provistos por la Intendencia.
    /// La imagen se graba en el directorio donde corre el test unitario (bin/Debug)
    /// </summary>
        [TestMethod()]
        public void jsonImageTest()
    {
        plate p = new plate() {plate_norm = "TEST",id_plate = 999,date = "01/01/2017 00:00" };
        consultaInfraccionesV2 ci =new consultaInfraccionesV2();

        string jsonString =
            Utilities.loadFile(Path.Combine(Utilities.BASE_DIR_RESPONSES, "response_json_unit_test.htm"));
        jsonResponse[] jr= JsonConvert.DeserializeObject<jsonResponse[]>(jsonString);
        //recorrer cada objeto e iterar
        int counter = 0;
        foreach (jsonResponse  r in jr)
        {
            //si el comando es insert, en data se encuentra la imagen html
            if (r.command == "insert")
            {
                //Chequear si tenemos data, en ese caso extraer la imagen
                if (r.data != "")
                {
                    //Tenemos una imagen para convertir,esto genera la imagen img_TESTMAT_201701010000_0.jpg
                    ci.saveImagesJpg(r.data,p,counter, Directory.GetCurrentDirectory() , false);
                    counter++;
                }
            }
        }
        Assert.IsTrue(true);

    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="msg"></param>
    private void CI_Msg(object sender, string msg)
    {
        Utilities.Log(msg);
    }
}
