﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void reverseDateTest_001()
        {
            string mydate = "10/01/2017 08:25";
            //Assert.AreEqual("uno", "dos");
            Assert.AreEqual("201701100825", Utilities.reversedDate(mydate));
        }

        [TestMethod]
        public void reverseDateTest_002()
        {
            string mydate = "3/5/2017 08:25";
            //Assert.AreEqual("uno", "dos");
            Assert.AreEqual("201705030825", Utilities.reversedDate(mydate));
        }

        [TestMethod]
        public void getPageTest_001()
        {
            string sample = "http://www.impo.com.uy/bases/notificaciones-cgm/2-2016/1";
   
            Assert.AreEqual(2,Utilities.getCounter(sample));
        }

        [TestMethod]
        public void getPageTest_002()
        {
            string sample = "http://www.impo.com.uy/bases/notificaciones-cgm/50-2016/1";

            Assert.AreEqual(50,Utilities.getCounter(sample));
        }


        [TestMethod]
        public void getPageTest_003()
        {
            string sample = "http://www.impo.com.uy/bases/notificaciones-cgm/179-2016/1";

            Assert.AreEqual(179,Utilities.getCounter(sample));
        }

        /// <summary>
        /// In case an invalid value, return -2
        /// </summary>
        [TestMethod]
        public void getPageTest_004()
        {
            string sample = "http://www.impo.com.uy/bases/notificaciones-cgm/hola-2016/1";

            Assert.AreEqual(-2, Utilities.getCounter(sample));
        }

        /// <summary>
        /// VERY SUBTLE change: if url does not match, return -1
        /// </summary>
        [TestMethod]
        public void getPageTest_005()
        {
            string sample = "http://www.impo.com.uy/bases/notificaciones-cgm/2-2016/3";

            Assert.AreEqual(-1, Utilities.getCounter(sample));
        }

        /*
                [TestMethod]
                public void reverseDateTest_004()
                {
                    string mydate = "4/x/2017 8:05";
                    //Assert.AreEqual("uno", "dos");
                    //Assert.AreEqual("201707040805", Utilities.reversedDate(mydate));
                    AssertFailedException("201707040805", Utilities.reversedDate(mydate));
                }
                */

    }
}
