﻿using System.IO;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Tesseract;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest2
    {
        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void speedTest()
        {

            TesseractEngine engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default);
            //engine.DefaultPageSegMode = Tesseract.PageSegMode.SingleChar;
            string imageTest = Path.Combine(Utilities.BASE_DIR_IMAGES , "img_AAA_9087_201702041520_0.jpg");
            string line =tesseractTest.Program.processImg(imageTest, engine,true);
            string stop_here = "stop";
        }

        [TestMethod]
        public void lightTest()
        {

            TesseractEngine engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default);
            //engine.DefaultPageSegMode = Tesseract.PageSegMode.SingleChar;
            string imageTest = Path.Combine(Utilities.BASE_DIR_IMAGES, "img_10820_201701112308_0.jpg");
            string line = tesseractTest.Program.processImg(imageTest, engine,true);
            string stop_here = "stop";
        }

        //img_AAD_3677_201701241253_0.jpg
        //Analisis de un caso particular que se repite muchas veces.
        [TestMethod]
        public void speedTest2()
        {

            TesseractEngine engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default);
            //engine.DefaultPageSegMode = Tesseract.PageSegMode.SingleChar;
            string imageTest = Path.Combine(Utilities.BASE_DIR_IMAGES, "img_AAD_3677_201701241253_0.jpg");
            string line = tesseractTest.Program.processImg(imageTest, engine,true);
            string stop_here = "stop";
        }
        //img_ATB_016_201612310805_0.jpg
        /// <summary>
        /// Hay un problemita con el código 32615...
        /// </summary>
        [TestMethod]
        public void speedTest3()
        {

            TesseractEngine engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default);
            //engine.DefaultPageSegMode = Tesseract.PageSegMode.SingleChar;
            string imageTest = Path.Combine(Utilities.BASE_DIR_IMAGES, "img_ATB_016_201612310805_0.jpg");
            string line = tesseractTest.Program.processImg(imageTest, engine, true);
            string stop_here = "stop";
        }

    }
}
