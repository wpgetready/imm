﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntAutomatizer;


namespace UnitTestingForIMM
{
    using NUnit.Framework;
    [TestFixture]
    class reverse_date_test
    {
        [Test]
        public void reverseDateTest()
        {
            string mydate = "10/01/2017 08:25";
            Assert.AreEqual("201701100825", Utilities.reversedDate(mydate));
        }
    }
}
