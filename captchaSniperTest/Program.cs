﻿using System;
using System.Text;
using System.Drawing;
using System.Net;
using System.IO;
using System.Drawing.Imaging;
using System.Threading;
using System.Windows.Forms;

namespace captchaSniperTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string captchaTest = @"D:\temp\image_captcha.jpg"; //Answer: 200|593430|1539|0|0|H5GmA
            //Nota 1: segun entiendo usuario y password no se usan porque esto es un simulador del servicio Decaptcha
            CaptchaSniper cs = new CaptchaSniper("none", "none");
            CSResult result= cs.GetCaptchaSolved(captchaTest,true);
            Console.WriteLine("MajorId={0},MinorId={1},resultCode={2},Text={3},Timeout={4}", result.MajorID, result.MinorID, result.ResultCode, result.Text, result.Timeout);
            Console.ReadLine();
        }
    }
}

/// <summary>
/// This class does way more than I need, so it was simplified a bit.
/// </summary>
    class CaptchaSniper
    {

        private string Uri = "http://127.0.0.1:80/";

        private string username = "";
        private string password = "";
        private string referer = "";
        private CSResult lastResult;
        private byte[] lastBmp;
        private bool flag;

        /// <summary>
        /// Creates new instance of Decaptcher class
        /// </summary>
        /// <param name="_username">username for users decaptcher account</param>
        /// <param name="_password">password for users decaptcher account</param>
        public CaptchaSniper(string _username, string _password)
        {
            username = _username;
            password = _password;
        }

        /// <summary>
        /// Creates new instance of Decaptcher class
        /// </summary>
        /// <param name="_username">username for users decaptcher account</param>
        /// <param name="_password">password for users decaptcher account</param>
        /// <param name="_referer">referer ID, if you have setup a affilate account.</param>
        public CaptchaSniper(string _username, string _password, string _referer)
        {
            referer = _referer;
            username = _username;
            password = _password;
        }

/* this solution works, however, I need FIRST to get the image and then process it.
 * The reason is simple: while more captcha are requested, more will be solved.
 * Unsolved one will going to a pool, to be manually solved if I wanted.
 * future versions, will check first solved solutions , and if there is already solved, provide the answer.
 * Yeah it can be gigantic but ratio will be greater along the time.
 */
        public CSResult GetCaptchaSolved(string FileUrl)
        {
            HttpWebRequest request = ((HttpWebRequest)(WebRequest.Create(FileUrl)));
            request.Method = "GET";
            HttpWebResponse response = ((HttpWebResponse)(request.GetResponse()));
            Stream requestStream = response.GetResponseStream();
            int streamLength = Convert.ToInt32(response.ContentLength);
            byte[] fileData = new byte[] {
                Convert.ToByte(streamLength)};
            requestStream.Read(fileData, 0, streamLength);
            requestStream.Close();
            return GetCaptchaSolved(fileData);
        }

        /// <summary>
        /// Custom-made solution to solve local files.
        /// Notice that localFile is never used, is to take advantage of overloading since there is another method with same parameter if I use string
        /// </summary>
        /// <param name="imagePath"></param>
        /// <param name="localFile"></param>
        /// <returns></returns>
        public CSResult GetCaptchaSolved(string imagePath, bool localFile)
        {
            Bitmap img = (Bitmap)Image.FromFile(imagePath);
            return GetCaptchaSolved(img);
        }

        public CSResult GetCaptchaSolved(byte[] File)
        {
            try
            {
                HttpWebRequest request = ((HttpWebRequest)(WebRequest.Create(Uri)));
                request.Method = "POST";
                request.KeepAlive = true;

                string boundary = ("-------------------------" + DateTime.Now.Ticks.ToString("x"));
                string header = ("\r\n" + ("--"
                            + (boundary + "\r\n")));
                string footer = ("\r\n" + ("--"
                            + (boundary + "\r\n")));
                request.ContentType = string.Format("multipart/form-data; boundary={0}", boundary);
                StringBuilder contents = new StringBuilder();
                contents.Append("\r\n");
                contents.Append(header);
                contents.Append(("Content-Disposition: form-data; name=\"function\"" + "\r\n"));
                contents.Append("\r\n");
                contents.Append("picture2");
                contents.Append(header);
                contents.Append(("Content-Disposition: form-data; name=\"username\"" + "\r\n"));
                contents.Append("\r\n");
                contents.Append(username);
                contents.Append(header);
                contents.Append(("Content-Disposition: form-data; name=\"password\"" + "\r\n"));
                contents.Append("\r\n");
                contents.Append(password);
                contents.Append(header);
                contents.Append(("Content-Disposition: form-data; name=\"pict_to\"" + "\r\n"));
                contents.Append("\r\n");
                contents.Append("0");
                contents.Append(header);
                contents.Append(("Content-Disposition: form-data; name=\"pict_type\"" + "\r\n"));
                contents.Append("\r\n");
                contents.Append(referer);
                contents.Append(header);
                contents.Append(("Content-Disposition: form-data; name=\"pict\"; filename=\"Untitled.jpg\"" + "\r\n"));
                contents.Append(("Content-Type: image/jpeg" + "\r\n"));
                contents.Append("\r\n");
                byte[] BodyBytes = Encoding.UTF8.GetBytes(contents.ToString());
                byte[] footerBytes = Encoding.UTF8.GetBytes(footer);
                request.ContentLength = (BodyBytes.Length
                            + (File.Length + footerBytes.Length));
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(BodyBytes, 0, BodyBytes.Length);
                requestStream.Write(File, 0, File.Length);
                requestStream.Write(footerBytes, 0, footerBytes.Length);
                requestStream.Flush();
                requestStream.Close();
                request.Timeout = 300000;
                string ret = (new StreamReader(request.GetResponse().GetResponseStream()).ReadToEnd());
                return new CSResult(ret.Split('|')[0], ret.Split('|')[1], ret.Split('|')[2], ret.Split('|')[3], ret.Split('|')[4], ret.Split('|')[5]);
            }
            catch
            {
                return null;
            }
        }

    /*FZSM Discarded. This is using a DecaptcherResult class, which I don't currently have.
        public CSResult ClaimBad(DecaptcherResult dr)
        {
            HttpWebRequest request = ((HttpWebRequest)(WebRequest.Create(Uri)));
            request.Method = "POST";
            request.KeepAlive = true;
            string boundary = ("-------------------------" + DateTime.Now.Ticks.ToString("x"));
            string header = ("\r\n" + ("--"
                        + (boundary + "\r\n")));
            string footer = ("\r\n" + ("--"
                        + (boundary + "\r\n")));
            request.ContentType = string.Format("multipart/form-data; boundary={0}", boundary);
            StringBuilder contents = new StringBuilder();
            contents.Append("\r\n");
            contents.Append(header);
            contents.Append(("Content-Disposition: form-data; name=\"function\"" + "\r\n"));
            contents.Append("\r\n");
            contents.Append("picture_bad2");
            contents.Append(header);
            contents.Append(("Content-Disposition: form-data; name=\"username\"" + "\r\n"));
            contents.Append("\r\n");
            contents.Append(username);
            contents.Append(header);
            contents.Append(("Content-Disposition: form-data; name=\"password\"" + "\r\n"));
            contents.Append("\r\n");
            contents.Append(password);
            contents.Append(header);
            contents.Append(("Content-Disposition: form-data; name=\"major_id\"" + "\r\n"));
            contents.Append("\r\n");
            contents.Append(dr.MajorID);
            contents.Append(header);
            contents.Append(("Content-Disposition: form-data; name=\"minor_id\"" + "\r\n"));
            contents.Append("\r\n");
            contents.Append(dr.MinorID);
            byte[] BodyBytes = Encoding.UTF8.GetBytes(contents.ToString());
            byte[] footerBytes = Encoding.UTF8.GetBytes(footer);
            request.ContentLength = (BodyBytes.Length + footerBytes.Length);
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(BodyBytes, 0, BodyBytes.Length);
            requestStream.Write(footerBytes, 0, footerBytes.Length);
            requestStream.Flush();
            requestStream.Close();
            try
            {
                string ret = (new StreamReader(request.GetResponse().GetResponseStream()).ReadToEnd());
                return new CSResult(ret.Split('|')[0], ret.Split('|')[1], ret.Split('|')[2], ret.Split('|')[3], ret.Split('|')[4], ret.Split('|')[5]);
            }
            catch
            {
                return null;
            }
        }
  
        //This method is not needed: it is used for pay service Decaptcha
        public string GetBalance()
        {
            HttpWebRequest request = ((HttpWebRequest)(WebRequest.Create(Uri)));
            request.Method = "POST";
            request.KeepAlive = true;
            string boundary = ("-------------------------" + DateTime.Now.Ticks.ToString("x"));
            string header = ("\r\n" + ("--"
                        + (boundary + "\r\n")));
            string footer = ("\r\n" + ("--"
                        + (boundary + "\r\n")));
            request.ContentType = string.Format("multipart/form-data; boundary={0}", boundary);
            StringBuilder contents = new StringBuilder();
            contents.Append("\r\n");
            contents.Append(header);
            contents.Append(("Content-Disposition: form-data; name=\"function\"" + "\r\n"));
            contents.Append("\r\n");
            contents.Append("balance");
            contents.Append(header);
            contents.Append(("Content-Disposition: form-data; name=\"username\"" + "\r\n"));
            contents.Append("\r\n");
            contents.Append(username);
            contents.Append(header);
            contents.Append(("Content-Disposition: form-data; name=\"password\"" + "\r\n"));
            contents.Append("\r\n");
            contents.Append(password);
            byte[] BodyBytes = Encoding.UTF8.GetBytes(contents.ToString());
            byte[] footerBytes = Encoding.UTF8.GetBytes(footer);
            request.ContentLength = (BodyBytes.Length + footerBytes.Length);
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(BodyBytes, 0, BodyBytes.Length);
            requestStream.Write(footerBytes, 0, footerBytes.Length);
            requestStream.Flush();
            requestStream.Close();
            string ret = (new StreamReader(request.GetResponse().GetResponseStream()).ReadToEnd());
            return ret;
        }
              */

    private byte[] BmpToBytes(Bitmap bmp)
        {
            MemoryStream m = new MemoryStream();
            bmp.Save(m, ImageFormat.Jpeg);

            byte[] output = m.GetBuffer();
            bmp.Dispose();
            m.Close();

            return output;
        }


        public CSResult GetCaptchaSolved(Bitmap captchaBmp)
        {
            lastBmp = BmpToBytes(captchaBmp);

            flag = false;
            Thread n = new Thread(new ThreadStart(GetCaptchaSolvedThread));
            n.Start();

            while (!flag)
            {
                Application.DoEvents();
            }

            return lastResult;
        }


        private void GetCaptchaSolvedThread()
        {
            lastResult = GetCaptchaSolved(lastBmp);
            flag = true;
        }

    }

    class CSResult
    {
        private string _ResultCode, _MajorID, _MinorID, _Type, _Timeout, _Text;

        public CSResult(string resultCode, string majorID, string minorID, string type, string timeout, string text)
        {
            this._ResultCode = resultCode;
            this._MajorID = majorID;
            this._MinorID = minorID;
            this._Type = type;
            this._Timeout = timeout;
            this._Text = text;
        }


        public string ResultCode
        {
            get
            {
                return _ResultCode;
            }
            set
            {
                _ResultCode = value;
            }
        }


        public string MajorID
        {
            get
            {
                return _MajorID;
            }
            set
            {
                _MajorID = value;
            }
        }


        public string MinorID
        {
            get
            {
                return _MinorID;
            }
            set
            {
                _MinorID = value;
            }
        }


        public string Type
        {
            get
            {
                return _Type;
            }
            set
            {
                _Type = value;
            }
        }


        public string Timeout
        {
            get
            {
                return _Timeout;
            }
            set
            {
                _Timeout = value;
            }
        }


        public string Text
        {
            get
            {
                return _Text;
            }
            set
            {
                _Text = value;
            }
        }

    }
