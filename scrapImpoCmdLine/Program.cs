﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace scrapImpoCmdLine
{
    /// <summary>
    /// This is the command line version of scrapping Impo.
    /// The idea is to schedule this app to scan site in weekly basis.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            //Connect events with the form
            callImpo ci = new callImpo();
            ci.Msg += new MessageEventHandler(receiveMsg);
            ci.start();
        }

        static void receiveMsg(object sender, string msg)
        {
            Utilities.Log(msg);
        }
    }
}
