﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// El objetivo es simple: hacer un scrap de la Intendencia y obtener las fotos.
/// Está LEJOS de ser trivial.
/// </summary>
namespace scrapIntendenciaCmdLine
{
    class Program
    {
        static void Main(string[] args)
        {
            consultaInfraccionesV2 ci = new consultaInfraccionesV2();
            ci.Msg += receiveMsg;
            ci.Start();
        }

        private static void receiveMsg(object sender, string msg)
        {
            Utilities.Log(msg);
        }
    }
}
