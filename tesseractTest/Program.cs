﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using Tesseract;
using System.Collections.Generic;


namespace tesseractTest
{
    public class Program
    {

        //Version 2:
        //Esta pequeña aplicación crea el metadata para las fotos de la Intendencia:
        //Que hace? se dedica a buscar información dentro de la imagen en zonas bien definidas, y lee cada
        //campo , guardandolo en una linea.
        //Hay al menos tres tipos de lecturas: lecturas para exceso de velocidad (2 formatos) y lecturas para semaforos.
        //Este es el primer lanzamiento, asi que contemplo la posibilidad que no se de ninguno de los dos
        //y que aparezcan otros escenarios.

        //UPDATE: Despues de inspeccionar, encuentro que los lectores de exceso de velocidad tiene 2 formatos
        //diferentes (en particular el 31626 si recuerdo bien) lo que obliga a usar 3 archivos.

        public static void Main(string[] args)
        {
            //return areas to be scanned;
            //El concepto está bien: ahora, tiene que ser testeable si o si. Asi como está no puedo testearlo...
        
            string linea;
            string first5;
            int counter_current = 0;
            try
            {
                TesseractEngine engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default);
                foreach (var file in Directory.GetFiles(Utilities.BASE_DIR_IMAGES, "*.jpg"))
                {
                    //Get the image,store it
                    counter_current++;
                    linea=processImg(file, engine);
                    linea = linea.Replace("\n", "");
                    first5 = linea.Substring(0, 5);
                    switch (first5)
                    {
                        case "sped1":
                            Utilities.writeLine("metadata_speed1.txt", linea);
                            break;
                        case "sped2":
                            Utilities.writeLine("metadata_speed2.txt", linea);
                            break;

                        case "light":
                            Utilities.writeLine("metadata_light.txt", linea);
                            break;
                        default:
                            Utilities.writeLine("metadata_errores.txt", linea);
                            break;
                    }
                    //Chequear la primer parte de la linea

                    Console.WriteLine(string.Format("{0} - {1}", counter_current, linea));

                    //Save the line
                    linea = "";
                }

            }
            catch (Exception e)
            {
                Trace.TraceError(e.ToString());
                Console.WriteLine("Unexpected Error: " + e.Message);
                Console.WriteLine("Details: ");
                Console.WriteLine(e.ToString());
            }
            Console.Write("Press any key to continue . . . ");
            Console.ReadKey(true);
        }

        public static string processImg(string file, TesseractEngine engine)
        {
            return processImg(file, engine, false);
        }

        //Process image an all areas involved.
        public static string processImg(string file,TesseractEngine engine,bool saveBmp)
        {
            List<int[]> scanArea;
            Image img = Image.FromFile(file);
            //Reject image if it is too small
            if (!(img.Width > 260 && img.Height > 108)) return file + ",ERROR: Imagen incorrecta" ;
            //Decide what to scan, return scanArea. If it is null that's because we couldn't decide it.
            string picType = selectAreas(img, engine, out scanArea);
            if (scanArea == null) return file +",ERROR:No fue posible detectar el tipo de imagen";
            string txtLine = picType + "," + Path.GetFileName(file);
            //Ok, scane the area, in blocks, return the results and assembly a line results.
            foreach (int[] area in scanArea)
            {
                txtLine += "," + process_area(area, img, engine,saveBmp); 
            }
            img.Dispose(); //discard image after analyzed
            return txtLine.Replace("\n","") ;
        }

        /// <summary>
        /// Esta rutina provee en forma hardcoded, una manera de aislar cual es el metadata a estudiar de las fotos.
        /// </summary>
        /// <param name="img"></param>
        /// <param name="engine"></param>
        /// <param name="scanArea"></param>
        /// <returns></returns>
        private static string selectAreas(Image img, TesseractEngine engine, out List<int[]> scanArea)
        {
            List<int[]> scanSemaforo = deteccion_semaforo();
            int[] Hora_Speed = new int[] { 388, 0, 524, 57 };
            int[] Hora_Semaforo = new int[] { 288, 0, 434, 57 };  //Ubicación de la hora en este sistema de arrays.
            int[] Codigo = new int[] { 1548, 0, 1685, 55 };
            string textScanned = scanText(img, Hora_Speed, engine);
            if (textScanned == "hora")
            {
                //Aqui tenemos candidatos al exceso de velocidad. He descubierto que al menos hay dos casos (quizas mas)
                textScanned = scanText(img, Codigo, engine);
                if (textScanned=="codigo")
                {
                    scanArea = deteccion_speed_one();
                    return "sped1";
                }
                else
                {
                    scanArea = deteccion_speed_two();
                    return "sped2";
                }
            }
            else
            {
                textScanned = scanText(img, Hora_Semaforo, engine);
                if (textScanned == "hora")
                {
                    scanArea = scanSemaforo;
                    return "light";
                }
                else
                {
                    scanArea = null;
                    return "indet";
                }
            }
        }

        /// <summary>
        /// Giving an image and a rectangle, scan the area and return results.
        /// </summary>
        /// <param name="img"></param>
        /// <param name="Hora_Speed"></param>
        /// <param name="engine"></param>
        /// <returns></returns>
        private static string scanText(Image img, int[] Hora_Speed, TesseractEngine engine)
        {
            string textScanned = process_area(Hora_Speed, img, engine,false);
            textScanned = textScanned.Trim().Replace(" ", "").Replace(":", "").ToLower();
            return textScanned;
        }


        /// <summary>
        /// Directly process one area from the image
        /// 
        /// </summary>
        /// <param name="img"></param>
        /// <param name="engine"></param>
        /// <returns></returns>
        public static string process_area(int[] area,Image img, TesseractEngine engine,bool saveBmp)
        {
            //Console.WriteLine("procesando area:{0},{1},{2},{3}", area[0], area[1], area[2]-area[0], area[3]-area[1]);
            Rectangle crop = new Rectangle( area[0], area[1], area[2]-area[0], area[3]-area[1]);
            Bitmap bmp = new Bitmap(crop.Width, crop.Height);
            using (var gr = Graphics.FromImage(bmp))
            {
                gr.DrawImage(img, new Rectangle(0, 0, bmp.Width, bmp.Height), crop, GraphicsUnit.Pixel);
            }
            //testeo, desactivado para mejorar la performance
         if (saveBmp==true)
            {
                bmp.Save("trozo_" + area[0].ToString() + "_" +
                                    area[1].ToString() + "_" +
                                    area[2].ToString() + "_" +
                                    area[3].ToString() + ".bmp");
            }

            engine.DefaultPageSegMode = Tesseract.PageSegMode.SingleBlock;
            Page page = engine.Process(bmp);
            var text = page.GetText();

            //Disposing is MANDATORY, otherwise we'll end up in a out of memory error(!)
            page.Dispose();
            bmp.Dispose();
           // img.Dispose();
            return text;
        }

        /// <summary>
        /// Define specific areas to scan in a picture.
        /// </summary>
        /// <returns></returns>
        public static List<int[]> deteccion_speed_two()
        {
            int[] velocidad = new int[] { 0, 0, 260, 108 };
            int[] hora = new int[] { 524, 0, 740, 55 };
            int[] fecha = new int[] { 524, 55, 740, 106 };
            int[] foto_p1 = new int[] { 524, 106, 625, 168 };
            int[] foto_p2 = new int[] { 625, 106, 680, 168 };
            int[] limite = new int[] { 1002, 0, 1080, 55 };
            int[] tipo = new int[] { 1002, 55, 1170, 106 };
            int[] carril = new int[] { 1002, 55, 1170, 106 }; 
            int[] fix = new int[] { 1362, 0, 1480, 55 }; 
            int[] myint = new int[] { 1362, 55, 1480, 106 }; 
            int[] dir = new int[] { 1362, 106, 1410, 168 };
            int[] codigo = new int[] { 1770, 0, 1940, 55 }; 
            int[] dir1 = new int[] { 1770, 55, 2140, 106 };
            int[] dir2 = new int[] { 1770, 106, 2140, 168 };

            List<int[]> scanAreas = new List<int[]>();
            scanAreas.Add(velocidad);
            scanAreas.Add(hora);
            scanAreas.Add(fecha);
            scanAreas.Add(foto_p1);
            scanAreas.Add(foto_p2);
            scanAreas.Add(limite);
            scanAreas.Add(tipo);
            scanAreas.Add(carril);
            scanAreas.Add(fix);
            scanAreas.Add(myint);
            scanAreas.Add(dir);
            scanAreas.Add(codigo);
            scanAreas.Add(dir1);
            scanAreas.Add(dir2);
            return scanAreas;
        }

        /// <summary>
        /// El codigo 3261 tiene ademas de dos zonas adicionales, corridas las columnas de codigo y dir1 y dir 2
        /// </summary>
        /// <returns></returns>
        public static List<int[]> deteccion_speed_one()
        {
            int[] velocidad = new int[] { 0, 0, 260, 108 };
            int[] hora = new int[] { 524, 0, 740, 55 };
            int[] fecha = new int[] { 524, 55, 740, 106 };
            int[] foto_p1 = new int[] { 524, 106, 625, 168 };
            int[] foto_p2 = new int[] { 625, 106, 680, 168 };
            int[] limite = new int[] { 1002, 0, 1080, 55 };
            int[] tipo = new int[] { 1002, 55, 1170, 106 };
            int[] dir = new int[] { 1362, 106, 1410, 168 };
            int[] codigo = new int[] { 1694, 0, 1940, 55 };
            int[] dir1 = new int[] { 1694, 55, 2140, 106 };
            int[] dir2 = new int[] { 1694, 106, 2140, 168 };

            List<int[]> scanAreas = new List<int[]>();
            scanAreas.Add(velocidad);
            scanAreas.Add(hora);
            scanAreas.Add(fecha);
            scanAreas.Add(foto_p1);
            scanAreas.Add(foto_p2);
            scanAreas.Add(limite);
            scanAreas.Add(tipo);
            scanAreas.Add(dir);
            scanAreas.Add(codigo);
            scanAreas.Add(dir1);
            scanAreas.Add(dir2);
            return scanAreas;
        }


        public static List<int[]> deteccion_semaforo()
        {
            int[] velocidad = new int[] { 0, 0, 260, 108 };
            int[] hora = new int[] { 434 , 0, 637, 55 };
            int[] fecha = new int[] { 434, 55, 637, 106 };
            int[] foto_p1 = new int[] { 434, 106, 528, 168 };
            int[] foto_p2 = new int[] { 528, 106, 630, 168 };
            int[] limite = new int[] { 852, 0, 932, 55 };
            int[] tipo = new int[] { 852, 55, 1000, 106 };
            int[] carril = new int[] { 852, 106, 930, 168 };
            int[] amarillo = new int[] { 1285, 0, 1430, 55 };
            int[] rojo = new int[] { 1285, 55, 1430, 106 };
            int[] semaforo = new int[] { 1285, 106, 1430, 168 };
            int[] fix = new int[] { 1576,0,1698,55 };
            int[] P_int = new int[]{1576,55,1698,106 };
            int[] codigo = new int[] {1928,0,2184,55 };
            int[] dir1 = new int[] { 1928, 55, 2384, 106 };
            int[] dir2 = new int[] { 1928, 106, 2384, 168 };

            List<int[]> scanAreas = new List<int[]>();
            scanAreas.Add(velocidad);
            scanAreas.Add(hora);
            scanAreas.Add(fecha);
            scanAreas.Add(foto_p1);
            scanAreas.Add(foto_p2);
            scanAreas.Add(limite);
            scanAreas.Add(tipo);
            scanAreas.Add(carril);
            scanAreas.Add(amarillo);
            scanAreas.Add(rojo);
            scanAreas.Add(semaforo);
            scanAreas.Add(fix);
            scanAreas.Add(P_int);
            scanAreas.Add(codigo);
            scanAreas.Add(dir1);
            scanAreas.Add(dir2);
            return scanAreas;
        }

    }



}


/*
 * 
 * Algunas notas:
 * Es indiferente si es fondo negro con letras blancas o viceversa, funciona igual.
 * Codigo simplificado porque solo se lee un texto simple.
 * 
 * Ubicación de la hora
 * Hora: 524,0,740,55
 *  * Fecha: 524,55,740,106 
 * Foto # 524,106,625,168
 * Foto # parte 2: 625,106,680,168  
 * Limite km/h: 1002,0,1080,55
 * Tipo:1002,55,1170,106
 * Dir (^ o al reves que no se cual es): 1362,106,1410,168
 *  Codigo: 1694,0,1940,55
 *  Dir 1: 1694,55,1940,106
 *  Dir 2: 1694,106,1940,168
 *  
 *  
 *  Desafio: los sistemas de deteccion parecen tener al menos dos configuraciones:
 *  1 como detector de velocidad exclusivamente, otro como detector en los semáforos.
 *  Tambien puede ser probable que funcionen como ambos dependiendo del modo, de acuerdo a lo que
 *  escuché en una entrevista con el director de la IMM.
 *  El tema es que para ambos casos, los offsets son diferentes.
 *  
 * Si tengo dos offsets, no me aflije, porque use dos sistemas diferentes con un punto de referencia.
 * En caso contrario, tendremos problemas.
 * */
